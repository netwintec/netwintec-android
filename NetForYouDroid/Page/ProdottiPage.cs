﻿using System;
using Xamarin.Forms;

namespace NetForYouDroid
{
	public class ProdottiPage : ContentPage
	{
		public ProdottiPage (float w,float h)
		{
			StackLayout stack= new StackLayout();

			var scrollview = new ScrollView {
				Content = stack,
			};

			//this.BackgroundColor = Color.Blue;
			//this.Title = "NetForYou";
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);


			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Filter", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.White;
					th.TextColor = Color.White;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));

			Image immagineNetForYou = new Image { Aspect = Aspect.Fill };
			immagineNetForYou.Source = ImageSource.FromFile("tasto_net_for_you.png");
			immagineNetForYou.HeightRequest = (w*307)/481;
			immagineNetForYou.WidthRequest = w;

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += OnTapGestureRecognizerTappedN4U;
			immagineNetForYou.GestureRecognizers.Add(tapGestureRecognizer);


			Image immagineBeIn = new Image { Aspect = Aspect.Fill };
			immagineBeIn.Source = ImageSource.FromFile("tasto_be_in.png");
			immagineBeIn.HeightRequest = (w*307)/481;
			immagineBeIn.WidthRequest = w;

			var tapGestureRecognizer2 = new TapGestureRecognizer();
			tapGestureRecognizer2.Tapped += OnTapGestureRecognizerTappedBI;
			immagineBeIn.GestureRecognizers.Add(tapGestureRecognizer2);


			Image immagineCheckTrack = new Image { Aspect = Aspect.Fill};
			immagineCheckTrack.Source = ImageSource.FromFile("tasto_check_track.png");
			immagineCheckTrack.HeightRequest = (w*307)/481;
			immagineCheckTrack.WidthRequest = w;

			var tapGestureRecognizer3 = new TapGestureRecognizer();
			tapGestureRecognizer3.Tapped += OnTapGestureRecognizerTappedCT;
			immagineCheckTrack.GestureRecognizers.Add(tapGestureRecognizer3);

			stack.Children.Add (immagineNetForYou);
			stack.Children.Add (immagineBeIn);
			stack.Children.Add (immagineCheckTrack);

			//stack.Padding = new Thickness (10, 10, 10, 10);

			this.Content = scrollview;
		}

		void OnTapGestureRecognizerTappedN4U(object sender, EventArgs args)
		{
			Navigation.PushAsync(new SitoPage("NetForYou","http://netforyou.netwintec.com"));
		}

		void OnTapGestureRecognizerTappedBI(object sender, EventArgs args)
		{
			Navigation.PushAsync(new SitoPage("BeIn","http://bein.netwintec.com"));
		}

		void OnTapGestureRecognizerTappedCT(object sender, EventArgs args)
		{
			Navigation.PushAsync(new SitoPage("CheckTrack","http://checktrack.netwintec.com"));
		}
	}
}

