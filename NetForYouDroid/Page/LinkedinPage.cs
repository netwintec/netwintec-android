﻿using System;

using Xamarin.Forms;

namespace NetForYouDroid
{
	public class LinkedinPage : ContentPage
	{
		public LinkedinPage ()
		{
			StackLayout stack = new StackLayout ();

			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Back", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.Transparent;
					th.TextColor = Color.Transparent;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));


			//this.BackgroundColor = Color.Blue;
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);
		
			StackLayout stackHorizontal1 = new StackLayout ();
			stackHorizontal1.Orientation = StackOrientation.Horizontal;

			Image immagine = new Image { Aspect = Aspect.AspectFit };
			immagine.Source = ImageSource.FromFile("jhonnybardella.png");

			Image immagine2 = new Image { Aspect = Aspect.AspectFit };
			immagine2.Source = ImageSource.FromFile("LinkedIn_Icon.png");
			var tapGestureRecognizerJhonny = new TapGestureRecognizer();
			tapGestureRecognizerJhonny.Tapped += delegate {

				Device.OpenUri(new Uri("https://it.linkedin.com/in/jhonnybardella"));

			};
			immagine2.GestureRecognizers.Add(tapGestureRecognizerJhonny);

			stackHorizontal1.Children.Add (immagine);

			StackLayout stackVertical1 = new StackLayout ();
			stackVertical1.Orientation = StackOrientation.Vertical;

			MyQuicksandRegularLabel Nome1 = new MyQuicksandRegularLabel {
				Text = "Jhonny",
				TextColor=Color.FromHex("3D3D3F"),
				//BackgroundColor= Color.FromHex("3D3D3F"),
				Font = Font.SystemFontOfSize (25),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
			};

			MyQuicksandRegularLabel Cognome1 = new MyQuicksandRegularLabel {
				Text = "Bardella",
				TextColor=Color.FromHex("3D3D3F"),
				//BackgroundColor= Color.FromHex("3D3D3F"),
				Font = Font.SystemFontOfSize (25),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
			};


			stackHorizontal1.HorizontalOptions = LayoutOptions.Center;

			stackVertical1.Children.Add (Nome1);
			stackVertical1.Children.Add (Cognome1);
			stackVertical1.Children.Add (immagine2);

			stackVertical1.HorizontalOptions = LayoutOptions.Center;
			stackVertical1.VerticalOptions = LayoutOptions.Center;

			stackHorizontal1.Children.Add (stackVertical1);

			StackLayout stackHorizontal2 = new StackLayout ();
			stackHorizontal2.Orientation = StackOrientation.Horizontal;

			Image immagine3 = new Image { Aspect = Aspect.AspectFit };
			immagine3.Source = ImageSource.FromFile("massimozennaro.png");

			Image immagine4 = new Image { Aspect = Aspect.AspectFit };
			immagine4.Source = ImageSource.FromFile("LinkedIn_Icon.png");
			var tapGestureRecognizerMassimo = new TapGestureRecognizer();
			tapGestureRecognizerMassimo.Tapped += delegate {

				Device.OpenUri(new Uri("https://it.linkedin.com/pub/massimo-zennaro/46/4a6/896"));

			};
			immagine4.GestureRecognizers.Add(tapGestureRecognizerMassimo);


			StackLayout stackVertical2 = new StackLayout ();
			stackVertical2.Orientation = StackOrientation.Vertical;

			MyQuicksandRegularLabel Nome2 = new MyQuicksandRegularLabel {
				Text = "Massimo",
				TextColor=Color.FromHex("3D3D3F"),
				//BackgroundColor= Color.FromHex("3D3D3F"),
				Font = Font.SystemFontOfSize (25),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
			};

			MyQuicksandRegularLabel Cognome2 = new MyQuicksandRegularLabel {
				Text = "Zennaro",
				TextColor=Color.FromHex("3D3D3F"),
				//BackgroundColor= Color.FromHex("3D3D3F"),
				Font = Font.SystemFontOfSize (25),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
			};


			stackHorizontal2.HorizontalOptions = LayoutOptions.Center;

			stackVertical2.Children.Add (Nome2);
			stackVertical2.Children.Add (Cognome2);
			stackVertical2.Children.Add (immagine4);

			stackVertical2.HorizontalOptions = LayoutOptions.Center;
			stackVertical2.VerticalOptions = LayoutOptions.Center;

			stackHorizontal2.Children.Add (stackVertical2);
			stackHorizontal2.Children.Add (immagine3);
			stackHorizontal2.Padding = new Thickness (0, 30, 0, 0);

			stack.Children.Add (stackHorizontal1);
			stack.Children.Add (stackHorizontal2);

			stack.Padding = new Thickness (10, 0, 10, 0);
			stack.VerticalOptions = LayoutOptions.Center;

			//stack.VerticalOptions = LayoutOptions.Center;

			this.Content = stack;



		}
	}
}


