﻿using System;
using Xamarin.Forms;

namespace NetForYouDroid
{
	public class NewsPage : ContentPage
	{
		public NewsPage (bool flag,float h,float w)
		{

			StackLayout stack= new StackLayout();
			stack.HorizontalOptions = LayoutOptions.FillAndExpand;

			var scrollview = new ScrollView {
				Content = stack,
			};

			//this.BackgroundColor = Color.Blue;
			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Back", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.White;
					th.TextColor = Color.White;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));


			//this.BackgroundColor = Color.Blue;
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);

			string url, titolo, descrizione,data;

			if(flag == true){
				titolo = App.Instance.Tit1;
				url = App.Instance.URL1;
				descrizione = App.Instance.Descr1;
				data = App.Instance.Data1;
			}
			else{
				titolo = App.Instance.Tit2;
				url = App.Instance.URL2;
				descrizione = App.Instance.Descr2;
				data = App.Instance.Data2;
			}
			Image immagine = new Image { Aspect = Aspect.AspectFit};
			immagine.Source = ImageSource.FromUri(new Uri (url));
			immagine.HeightRequest = (h * 10) / 21;

			MyQuicksandBoldLabel Titolo = new MyQuicksandBoldLabel {
				Text = titolo,
				TextColor = Color.FromHex("3D3D3F"),
				Font = Font.SystemFontOfSize (20),
				//TranslationX = 10,
				//HorizontalOptions = LayoutOptions.Center,
			};

			MyQuicksandLightLabel Data = new MyQuicksandLightLabel {
				Text = data,
				TextColor = Color.FromHex("575854"),
				Font = Font.SystemFontOfSize (18),
				//TranslationX = 10,
				//HorizontalOptions = LayoutOptions.Center,
			};
				

			MyQuicksandRegularLabel Descrizione = new MyQuicksandRegularLabel {
				Text = descrizione,
				TextColor = Color.FromHex("3D3D3F"),
				Font = Font.SystemFontOfSize (15),
				TranslationY = 10
			};

			StackLayout stackNews= new StackLayout();
			stackNews.Padding = new Thickness (20, 0, 20, 0);
			stackNews.Children.Add (Titolo);
			stackNews.Children.Add (Data);
			stackNews.Children.Add (Descrizione);

			stack.Children.Add (immagine);
			stack.Children.Add (stackNews);

			stack.Padding = new Thickness (0, 0, 0, 0);

			this.Content = scrollview;

		}
	}
}

