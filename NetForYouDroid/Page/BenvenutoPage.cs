﻿using System;

using Xamarin.Forms;

namespace NetForYouDroid
{
	public class BenvenutoPage : ContentPage
	{
		public BenvenutoPage (int h, int w)
		{

			StackLayout stack = new StackLayout ();

			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Back", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.Transparent;
					th.TextColor = Color.Transparent;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));


			//this.BackgroundColor = Color.Blue;
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);

			Image immagine = new Image { Aspect = Aspect.AspectFit };
			immagine.Source = ImageSource.FromFile("benvenuti_netwintec_01.png");

			stack.Children.Add (immagine);



			stack.Padding = new Thickness (10, 0, 10, 0);
			DateTime dt = DateTime.Now;

			string giorno="";
			switch(dt.DayOfWeek.ToString()){

			case "Monday":
				giorno = "Lunedì";
				break;
			case "Tuesday":
				giorno = "Martedì";
				break;
			case "Wednesday":
				giorno = "Mercoledì";
				break;
			case "Thursday":
				giorno = "Giovedì";
				break;
			case "Friday":
				giorno = "Venerdì";
				break;
			case "Saturday":
				giorno = "Sabato";
				break;
			case "Sunday":
				giorno = "Domenica";
				break;
				
			}

			string Data = giorno + "  " + dt.Day + "/" + dt.Month + "/" + dt.Year;

			MyQuicksandRegularLabel LabelData = new MyQuicksandRegularLabel {
				Text = Data,
				TextColor=Color.FromHex("3D3D3F"),
				HeightRequest = h*0.0625,
				WidthRequest = w,
				//BackgroundColor= Color.FromHex("3D3D3F"),
				Font = Font.SystemFontOfSize (25),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				//HorizontalOptions = LayoutOptions.Center,
			};
			// Build the page.

			stack.Children.Add (LabelData);

			stack.VerticalOptions = LayoutOptions.Center;

			this.Content = stack;


		}
	}
}


