﻿using System;
using Xamarin.Forms;

namespace NetForYouDroid
{
	public class MainLoginPage : ContentPage
	{
		float widht;
		float height;
		public int play =0;
		bool flag= true;
		AbsoluteLayout absoluteLayout;
		Image immagineNetForYou; 
		MyQuicksandRegularLabel bottoneLogin,bottoneRegistrati;
		public MainLoginPage (float w,float h)
		{

			widht=w;
			height = h;
			Command<Type> navigateCommand =
				new Command<Type>(async (Type pageType) =>
					{
						Page page = (Page)Activator.CreateInstance(pageType);
						await this.Navigation.PushAsync(page);
					});
						
			NavigationPage.SetHasNavigationBar (this, false);
			absoluteLayout = new AbsoluteLayout{};

			immagineNetForYou = new Image { Aspect = Aspect.Fill };
			immagineNetForYou.Source = ImageSource.FromFile ("logo_pagina_iniziale.png");

			var fsReg = new FormattedString ();
			fsReg.Spans.Add (new Span { Text="R",  FontSize = 16 });
			fsReg.Spans.Add (new Span { Text="EGISTRATI", FontSize = 11 });

			var fsLogconFB = new FormattedString ();
			fsLogconFB .Spans.Add (new Span { Text="L",  FontSize =  16 });
			fsLogconFB .Spans.Add (new Span { Text="OGIN WITH ", FontSize = 11 });
			fsLogconFB .Spans.Add (new Span { Text="F",  FontSize = 16 });
			fsLogconFB .Spans.Add (new Span { Text="ACEBOOK", FontSize = 11 });

			bottoneLogin = new MyQuicksandRegularLabel {
				FormattedText = fsLogconFB,
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				TextColor=Color.White,
				//HeightRequest = 50,
				//WidthRequest = (widht/2)-2,
				BackgroundColor= Color.FromHex("3D3D3F"),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				//HorizontalOptions = LayoutOptions.Center,
			};

			var tapGestureRecognizer = new TapGestureRecognizer ();
			tapGestureRecognizer.Tapped += OnTapGestureRecognizerTapped;
			bottoneLogin.GestureRecognizers.Add (tapGestureRecognizer);

			bottoneRegistrati = new MyQuicksandRegularLabel {
				FormattedText = fsReg,
				//Text = App.Instance.Tit1,
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				TextColor=Color.White,
				//HeightRequest = 50,
				//WidthRequest = (widht/2)-2,
				BackgroundColor= Color.FromHex("3D3D3F"),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				//HorizontalOptions = LayoutOptions.Center,
			};

			var tapGestureRecognizer2 = new TapGestureRecognizer ();
			tapGestureRecognizer2.Tapped += OnTapGestureRecognizerTapped2;
			bottoneRegistrati.GestureRecognizers.Add (tapGestureRecognizer2);

			ViewBackground viewBackground = new ViewBackground ();

			var webView= new WebView();
			var htmlSource = new HtmlWebViewSource ();
			htmlSource.Html = @"<html>
									<head>
									    <script src='references/jquery-1.11.3.min.js'></script>
									    <script src='references/jquery-ui.min.js'></script>
									    <script src='references/video.js'></script>
									    <script src='references/imagesloaded.pkgd.min.js'></script>
									    <script src='references/bigvideo.js'></script>
										<script src='references/jquery.mobile-1.4.5.min.js'></script>	
										<script src='references/jquery.vide.js'></script>
									    <link rel='stylesheet' type='text/css' href='references/testCss.css'>
									</head>
									<body>
									</body>
								</html>";
			
			webView.Source = htmlSource;
			webView.IsEnabled = false;


			AbsoluteLayout.SetLayoutFlags (webView,
				AbsoluteLayoutFlags.None);

			AbsoluteLayout.SetLayoutBounds (webView,
				new Rectangle (0, 0,widht, height));

			
			AbsoluteLayout.SetLayoutFlags (immagineNetForYou,
				AbsoluteLayoutFlags.None);

			AbsoluteLayout.SetLayoutBounds (immagineNetForYou,
				new Rectangle ((widht/2)-(height/8), (height/8),(height/4), (height/4)));

			AbsoluteLayout.SetLayoutFlags (bottoneLogin,
				AbsoluteLayoutFlags.None);

			AbsoluteLayout.SetLayoutBounds (bottoneLogin,
				new Rectangle (1f, height-50-25, (widht/2)-2, 50));

			AbsoluteLayout.SetLayoutFlags (bottoneRegistrati,
				AbsoluteLayoutFlags.None);

			AbsoluteLayout.SetLayoutBounds (bottoneRegistrati,
				new Rectangle ((widht/2)+1, height-50-25, (widht/2)-2,50));

			absoluteLayout.Children.Add (webView);
			absoluteLayout.Children.Add (immagineNetForYou);
			absoluteLayout.Children.Add (bottoneRegistrati);
			absoluteLayout.Children.Add (bottoneLogin);

			Content = absoluteLayout;

		}

		void OnTapGestureRecognizerTapped(object sender, EventArgs args)
		{
			Navigation.PushModalAsync(new LoginPage());
		}

		void OnTapGestureRecognizerTapped2(object sender, EventArgs args)
		{
			var result = this.DisplayAlert("Cooming Soon", "Stiamo Lavorando Per Voi", "Exit");
			System.Diagnostics.Debug.WriteLine(result);
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
		}
	}
}

