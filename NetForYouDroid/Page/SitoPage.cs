﻿using System;
using Xamarin.Forms;
namespace NetForYouDroid
{
	public class SitoPage : ContentPage
	{
		public SitoPage (string titolo,string url)
		{
			StackLayout stack = new StackLayout ();
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);

			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Filter", "tasto_indietro_header.png", async () =>
				{
					if(titolo.CompareTo("NetWinTec")==0){
						App.Instance.SavePrec(1);
						TornaHomeAzienda th = new TornaHomeAzienda();
						th.BackgroundColor = Color.White;
						th.TextColor = Color.White;
						stack.Children.Add(th);
					}
					else{
						App.Instance.SavePrec(2);
						TornaHomeProdotti th = new TornaHomeProdotti();
						th.BackgroundColor = Color.White;
						th.TextColor = Color.White;
						stack.Children.Add(th);
					}
				}));

			WebView webView = new WebView
			{
				Source = new UrlWebViewSource
				{
					Url = url,

				},
				VerticalOptions = LayoutOptions.FillAndExpand

			};
			// Accomodate iPhone status bar.
			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

			stack.Children.Add (webView);
			// Build the page.
			this.Content = stack;

		}
	}
}

