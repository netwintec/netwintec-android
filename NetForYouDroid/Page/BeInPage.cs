﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace NetForYouDroid
{
	public class BeInPage : ContentPage
	{
		public BeInPage() {
			var stack = new StackLayout { Spacing = 0 };

			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Back", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.White;
					th.TextColor = Color.White;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));


			//this.BackgroundColor = Color.Blue;
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);

			var map = new MyMap(
				MapSpan.FromCenterAndRadius(
					new Position(43.842130, 11.173581), Distance.FromMeters(20))) {
				IsShowingUser = true, // iOS asks for permission if this is set to true, but not if not set or set to false.
				HeightRequest = 100,
				WidthRequest = 960,
				VerticalOptions = LayoutOptions.FillAndExpand,
				MapType = MapType.Satellite
			};

			stack.Children.Add(map);
			Content = stack;
		}
	}

	public class MyMap2 : Map {

		public MyMap2(MapSpan region) : base(region)
		{
		}
	}

	public class MyMap : Map {

		public MyMap(MapSpan region) : base(region)
		{
		}
	}
}

