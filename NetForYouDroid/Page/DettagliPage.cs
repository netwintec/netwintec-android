﻿using System;
using Xamarin.Forms;

namespace NetForYouDroid
{
	public class DettagliPage : ContentPage
	{
		public DettagliPage ()
		{
			this.Title = "NetForYou";

			StackLayout stack= new StackLayout();

			var scrollview = new ScrollView {
				Content = stack,
			};

			Label Header = new Label {
				Text = "Dettagli",
				Font = Font.SystemFontOfSize (NamedSize.Large),
				HorizontalOptions = LayoutOptions.Center,
			};

			stack.Children.Add (Header);
			this.Content = scrollview;
		}
	}
}

