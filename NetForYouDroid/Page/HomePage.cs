﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Labs.Controls;

namespace NetForYouDroid
{
	public class HomePage : ContentPage
	{
		int i=1;
		public float height;
		public float widht;

		public HomePage (float w,float h)
		{
			Command<Type> navigateCommand =
				new Command<Type>(async (Type pageType) =>
					{
						Page page = (Page)Activator.CreateInstance(pageType);
						await this.Navigation.PushAsync(page);
					});
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);
			NavigationPage.SetHasBackButton (this, false);

			height = h;
			widht = w;

			StackLayout stack= new StackLayout();

			var scrollview = new ScrollView {
				Content = stack,
			};
				


			AbsoluteLayout absoluteLayout = new AbsoluteLayout
			{
				BackgroundColor = Color.White,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = (height*0.42),
			};

			double f;
			if (h < 1000)
				f = 15;
			else
				f = 23;

			MyQuicksandRegularLabel Titolo = new MyQuicksandRegularLabel {
				Text = App.Instance.Tit1,
				TextColor=Color.White,
				HeightRequest = h*0.0625,
				WidthRequest = w,
				BackgroundColor= Color.FromHex("3D3D3F"),
				Font = Font.SystemFontOfSize (f),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				//HorizontalOptions = LayoutOptions.Center,
			};

			var tapGestureRecognizer3 = new TapGestureRecognizer ();
			tapGestureRecognizer3.Tapped += OnTapGestureRecognizerTapped;
			Titolo.GestureRecognizers.Add (tapGestureRecognizer3);

			Uri url1 = new Uri (App.Instance.URL1);
			Uri url2 = new Uri (App.Instance.URL2);

			Image immagineNetForYou = new Image { Aspect = Aspect.Fill };
			immagineNetForYou.Source = ImageSource.FromUri(url1);

			App.Instance.SaveNews(true);

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += OnTapGestureRecognizerTapped;
			immagineNetForYou.GestureRecognizers.Add(tapGestureRecognizer);


			Image immaginePrec = new Image { Aspect = Aspect.Fill };
			immaginePrec.Source = ImageSource.FromFile("freccia_sin.png");
			//immaginePrec.BackgroundColor = Color.White;
			/*
			Button bottonePrec= new Button 
			{
				Image = "freccia_sin.png",
				HorizontalOptions = LayoutOptions.Center,
				//Image = "img_evento.jpg",
				BackgroundColor = Color.Black,
				WidthRequest = height*0.104,
				HeightRequest =  height*0.104,
				//Opacity = 0.5
			};
			*/
			var tapGestureRecognizerPrec = new TapGestureRecognizer();
			tapGestureRecognizerPrec.Tapped += delegate {

				i++;
				if((i%2)==0){
					i=0;
					Titolo.Text=App.Instance.Tit2;
					immagineNetForYou.Source = ImageSource.FromUri(url2);
					App.Instance.SaveNews(false);
				}
				else{
					Titolo.Text=App.Instance.Tit1;
					immagineNetForYou.Source = ImageSource.FromUri(url1);
					App.Instance.SaveNews(true);
				}

			};
			immaginePrec.GestureRecognizers.Add(tapGestureRecognizerPrec);

			Image immagineNext = new Image { Aspect = Aspect.Fill };
			immagineNext.Source = ImageSource.FromFile("freccia_dex.png");
			//immagineNext.BackgroundColor = Color.White;

			var tapGestureRecognizerNext = new TapGestureRecognizer();
			tapGestureRecognizerNext.Tapped += delegate {

				i++;
				if((i%2)==0){
					i=0;
					Titolo.Text=App.Instance.Tit2;
					immagineNetForYou.Source = ImageSource.FromUri(url2);
					App.Instance.SaveNews(false);
				}
				else{
					Titolo.Text=App.Instance.Tit1;
					immagineNetForYou.Source = ImageSource.FromUri(url1);
					App.Instance.SaveNews(true);
				}

			};
			immagineNext.GestureRecognizers.Add(tapGestureRecognizerNext);


			System.Diagnostics.Debug.WriteLine ((height * 0.42)+"    "+(height*0.104));

			AbsoluteLayout.SetLayoutFlags (immagineNetForYou,
				AbsoluteLayoutFlags.None);

			AbsoluteLayout.SetLayoutBounds (immagineNetForYou,
				new Rectangle (0f, 0f,widht, (height*0.42)));

			AbsoluteLayout.SetLayoutFlags (immaginePrec,
				AbsoluteLayoutFlags.None);

			AbsoluteLayout.SetLayoutBounds (immaginePrec,
				new Rectangle (10f, (((height*0.42)/2)-(height*0.052)), height*0.104, height*0.104));

			AbsoluteLayout.SetLayoutFlags (immagineNext,
				AbsoluteLayoutFlags.None);

			AbsoluteLayout.SetLayoutBounds (immagineNext,
				new Rectangle ((widht-(height*0.104))-10, (((height*0.42)/2)-(height*0.052)), height*0.104, height*0.104));

			absoluteLayout.Children.Add (immagineNetForYou);
			absoluteLayout.Children.Add (immaginePrec);
			absoluteLayout.Children.Add (immagineNext);

			Grid myGrid = new Grid ();
			if(h<1000)
				myGrid.Padding = new Thickness (5, 5, 5, 5);
			else
				myGrid.Padding = new Thickness (0, 5, 0, 5);
			//myGrid.HorizontalOptions = LayoutOptions.FillAndExpand;

			myGrid.RowDefinitions = new RowDefinitionCollection
			{
				new RowDefinition { Height = new GridLength(((widht*15)/32)-10) },
				new RowDefinition { Height = new GridLength(((widht*15)/32)-10) },
			};

			myGrid.RowSpacing = 10;

			myGrid.ColumnDefinitions = new ColumnDefinitionCollection
			{
				new ColumnDefinition { Width = new GridLength((widht*15)/32)},
				new ColumnDefinition { Width = new GridLength((widht*15)/32)}

			};
			if(h>1000)
				myGrid.ColumnSpacing = 10;


			myGrid.HorizontalOptions = LayoutOptions.Center;
			myGrid.VerticalOptions = LayoutOptions.Center;

			Image bottoneAzienda= new Image 
			{
				Source = ImageSource.FromFile("icona_about_us_sfondo.png"),
				Aspect = Aspect.Fill,
				BackgroundColor = Color.FromHex("54C2E8"),
				HorizontalOptions = LayoutOptions.Center,
				//VerticalOptions = LayoutOptions.Center,
				WidthRequest =((widht*15)/32),
				HeightRequest = ((widht*15)/32)-10
			};
			var tapGestureRecognizerAzienda = new TapGestureRecognizer();
			tapGestureRecognizerAzienda.Tapped += delegate {

				Navigation.PushAsync (new AziendaPage (widht, height));

			};
			bottoneAzienda.GestureRecognizers.Add(tapGestureRecognizerAzienda);



			Image bottoneProdotti= new Image 
			{
				Source = ImageSource.FromFile("icona_prodotti_sfondo.png"),
				Aspect = Aspect.Fill,
				BackgroundColor = Color.FromHex("54C2E8"),
				HorizontalOptions = LayoutOptions.Center,
				//VerticalOptions = LayoutOptions.Center,
				WidthRequest = ((widht*15)/32),
				HeightRequest = ((widht*15)/32)-10,
			};
			var tapGestureRecognizerProdotti = new TapGestureRecognizer();
			tapGestureRecognizerProdotti.Tapped += delegate {

				Navigation.PushAsync (new ProdottiPage (widht, height));

			};
			bottoneProdotti.GestureRecognizers.Add(tapGestureRecognizerProdotti);

			Image bottoneRA= new Image 
			{
				Source = ImageSource.FromFile("icona_real_aumentata_sfondo.png"),
				Aspect = Aspect.Fill,
				BackgroundColor = Color.FromHex("54C2E8"),
				HorizontalOptions = LayoutOptions.Center,
				//VerticalOptions = LayoutOptions.Center,
				WidthRequest = ((widht*15)/32),
				HeightRequest = ((widht*15)/32)-10,
				//BorderWidth = 40,
				//BorderColor = Color.Red,
			};
			var tapGestureRecognizerRA = new TapGestureRecognizer();
			tapGestureRecognizerRA.Tapped += delegate {

				Navigation.PushAsync (new AugmentedReality());

			};
			bottoneRA.GestureRecognizers.Add(tapGestureRecognizerRA);

			Image bottoneContatti= new Image
			{
				Source = ImageSource.FromFile("icona_contatti_sfondo.png"),
				Aspect = Aspect.Fill,
				BackgroundColor = Color.FromHex("54C2E8"),
				HorizontalOptions = LayoutOptions.Center,
				//VerticalOptions = LayoutOptions.Center,
				WidthRequest = ((widht*15)/32),
				HeightRequest = ((widht*15)/32)-10,
			};

			var tapGestureRecognizerContatti = new TapGestureRecognizer();
			tapGestureRecognizerContatti.Tapped += delegate {

				Navigation.PushAsync (new ContattiPage(widht,height));

			};
			bottoneContatti.GestureRecognizers.Add(tapGestureRecognizerContatti);

			float wh, tr;
			if (h < 1000) {
				wh = widht - 15;
				tr = 0;
			} else {
				wh = widht - 40;
				tr = 0;
			}

			Image bottoneBeIn= new Image 
			{
				Source = ImageSource.FromFile("be_in_sfondo.png"),
				Aspect = Aspect.Fill,
				BackgroundColor = Color.FromHex("54C2E8"),
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = wh,
				HeightRequest = ((widht*15)/32)-10,
			};

			var tapGestureRecognizerBeIn = new TapGestureRecognizer();
			tapGestureRecognizerBeIn.Tapped += delegate {

				Navigation.PushAsync (new BeInPage());

			};
			bottoneBeIn.GestureRecognizers.Add(tapGestureRecognizerBeIn);

			bottoneBeIn.TranslationX = tr; 


			myGrid.Children.Add(bottoneRA, 0, 1);
			myGrid.Children.Add(bottoneAzienda, 1, 0);
			myGrid.Children.Add(bottoneProdotti, 0, 0);
			myGrid.Children.Add(bottoneContatti, 1, 1);

			//stack.Children.Add (Header);
			stack.Children.Add (absoluteLayout);
			stack.Children.Add (Titolo);
			stack.Children.Add (myGrid);
			stack.Children.Add (bottoneBeIn);
			stack.Padding = new Thickness (0, 0, 0, 0);

			this.Content = scrollview;
		}


		void OnTapGestureRecognizerTapped(object sender, EventArgs args)
		{
			System.Diagnostics.Debug.WriteLine (App.Instance.News);
			Navigation.PushAsync(new NewsPage(App.Instance.News,height,widht));
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			App.Instance.SaveScanBeIn(false);
			App.Instance.SaveScan (true);
		}
	}
}

