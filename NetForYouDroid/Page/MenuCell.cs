﻿using System;
using Xamarin.Forms;

namespace NetForYouDroid
{
	public class MenuCell : ViewCell
	{
		public string Text { 
			get { return label.Text; }
			set{ label.Text = value;} 
		}
		Label label;

		public MainDetailPage Host { get; set; }

		public MenuCell ()
		{
			label = new Label {
				YAlign = TextAlignment.Center,
				TextColor = Color.Black,
			};

			var layout = new StackLayout {
				BackgroundColor = App.HeaderTint,
				Padding = new Thickness(20, 0, 0, 0),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				Children = {label}
			};
			View = layout;
		}

		protected override void OnTapped ()
		{
			base.OnTapped ();

			Host.Selected (label.Text);
		}
	}
}

