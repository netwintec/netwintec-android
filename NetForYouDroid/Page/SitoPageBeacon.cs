﻿using Xamarin.Forms;
namespace NetForYouDroid
{
	public class SitoPageBeacon : ContentPage
	{
		public SitoPageBeacon ()
		{

			StackLayout stack = new StackLayout ();

			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Back", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.White;
					th.TextColor = Color.White;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));


			//this.BackgroundColor = Color.Blue;
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);

			WebView webView = new WebView
			{
				Source = new UrlWebViewSource
				{
					Url = "https://www.google.com/calendar/embed?src=netwintec.com_a5htekejqkohio5nihuoqgltpc%40group.calendar.google.com&ctz=Europe/Rome",
				},
				VerticalOptions = LayoutOptions.FillAndExpand

			};

			stack.Children.Add (webView);
			// Accomodate iPhone status bar.
			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

			// Build the page.
			Content = stack;
		}
	}
}

