﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace NetForYouDroid
{
	public class ContattiPage : ContentPage
	{
		public ContattiPage (float w,float h)
		{
			StackLayout stack= new StackLayout();

			var scrollview = new ScrollView {
				Content = stack,
			};

			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Back", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.White;
					th.TextColor = Color.White;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));


			//this.BackgroundColor = Color.Blue;
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);

			MyQuicksandBoldLabel Ditta = new MyQuicksandBoldLabel {
				Text = "NETWINTEC srl",
				Font = Font.SystemFontOfSize (24),
				TranslationX = 5,
				FontFamily = Device.OnPlatform (
					"Quicksand-Bold",
					null,
					null
				),
				TextColor = Color.FromHex("3D3D3F"),
				HeightRequest=50

					//FontAttributes = FontAttributes.Bold
			};

			MyQuicksandRegularLabel Indirizzo = new MyQuicksandRegularLabel {
				Text = "Via S. Morese, 54",
				Font = Font.SystemFontOfSize (NamedSize.Medium),
				TranslationX = 5,
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				//FontAttributes = FontAttributes.Bold
			};

			MyQuicksandRegularLabel Indirizzo2 = new MyQuicksandRegularLabel {
				Text = "50041 - Calenzano (FI)",
				Font = Font.SystemFontOfSize (NamedSize.Medium),
				TranslationX = 5,
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				//FontAttributes = FontAttributes.Bold
			};

			MyQuicksandRegularLabel web = new MyQuicksandRegularLabel {
				Text = "web: www.netwintec.com",
				Font = Font.SystemFontOfSize (NamedSize.Medium),
				TranslationX = 5,
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				//FontAttributes = FontAttributes.Bold
			};

			MyQuicksandRegularLabel email = new MyQuicksandRegularLabel {
				Text = "e-mail: info@netwintec.com",
				Font = Font.SystemFontOfSize (NamedSize.Medium),
				TranslationX = 5,
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
			};	

			var map = new Map(new MapSpan(new Position(43.842077, 11.173881), 0.05, 0.05))
			{
				MapType = MapType.Street,
				HeightRequest = (h*10)/21
			};
			map.BackgroundColor = Color.White;

			Pin pin;
			map.Pins.Add(pin = new Pin()
				{
					Label = "NetWinTec", 
					Position = new Position(43.842077, 11.173881),
					Type = PinType.Place
				});	

			StackLayout stack2= new StackLayout();
			stack2.Padding = new Thickness (20, 0, 0, 10);
			stack2.Children.Add (Ditta);
			stack2.Children.Add (Indirizzo);
			stack2.Children.Add (Indirizzo2);
			stack2.Children.Add (web);
			stack2.Children.Add (email);


			stack.Children.Add (map);
			stack.Children.Add (stack2);
			//stack.Children.Add (Sito);

			stack.Padding = new Thickness (0, 0, 0, 10);

			this.Content = scrollview;
		}
	}
}

