﻿using System;
using Xamarin.Forms;

namespace NetForYouDroid
{
	public class MenuTableView : TableView 
	{
	}

	public class MainDetailPage : ContentPage
	{
		MasterDetailPage master;

		TableView tableView;
		public float widht,height;

		public MainDetailPage (MasterDetailPage m,float w,float h)
		{
			widht = w;
			height = h;

			master = m;

			Title = "NetForYou";
			Icon = "tasto_menu.png";



			var section = new TableSection () {
				new MenuCell {Text = "Home", Host = this},
				new MenuCell {Text = "News", Host = this},
				new MenuCell {Text = "Prodotti", Host = this},
				new MenuCell {Text = "AboutUs", Host = this},
				new MenuCell {Text = "Realtà Aumentata", Host = this},
				new MenuCell {Text = "Contatti", Host = this},
				new MenuCell {Text = "BeIn", Host = this},
				new MenuCell {Text = "Logout", Host = this}
			};

			var root = new TableRoot () {section} ;

			tableView = new MenuTableView ()
			{ 
				Root = root,
				//				HeaderTemplate = new DataTemplate (typeof(MenuHeader)),
				Intent = TableIntent.Menu,
			};


			this.Content = new StackLayout {
				BackgroundColor = Color.White,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = {tableView}
			};
		}

		NavigationPage home,prodotti,contatti,azienda,news,RealtàAumentata,BeIn,Logout;
		public void Selected (string item) {

			switch (item) {

			case "Home":
				//if (home == null)
				home = new NavigationPage (new HomePage (widht,height)) { Tint = App.NavTint };
				master.Detail = home;
				break;
			
			case "News":
				//if (news == null)
					news = new NavigationPage (new NewsPage (App.Instance.News,height,widht)) { Tint = App.NavTint };
				master.Detail = news;
				break;

			case "Prodotti":
				//if (prodotti == null) 
					prodotti = new NavigationPage (new ProdottiPage (widht,height)) { Tint = App.NavTint };

				master.Detail = prodotti;
				break;

			case "AboutUs":
				//if (azienda == null)
					azienda = new NavigationPage (new AziendaPage (widht,height)) { Tint = App.NavTint };
				master.Detail = azienda;
				break;

			case "Realtà Aumentata":
				//if (RealtàAumentata == null)
					RealtàAumentata = new NavigationPage (new AugmentedReality ()) { Tint = App.NavTint };
				master.Detail = RealtàAumentata;
				break;

			case "Contatti":
				//if (contatti == null)
					contatti = new NavigationPage (new ContattiPage (widht,height)) { Tint = App.NavTint };
				master.Detail = contatti;
				break;

			case "BeIn":
				//if (BeIn == null)
					BeIn = new NavigationPage (new BeInPage ()) { Tint = App.NavTint };
				master.Detail = BeIn;
				break;

			case "Logout":
				//if (Logout == null)
					Logout = new NavigationPage (new LogoutPage ()) { Tint = App.NavTint };
				master.Detail = Logout;
				break;
			};

			master.IsPresented = false;  // close the slide-out
		}
	}
}

