﻿using System;
using Xamarin.Forms;

namespace NetForYouDroid
{
	public class AziendaPage : ContentPage
	{
		MyQuicksandRegularLabel TestoNetwork,TestoMission,TestoVision;
		public int Indice1=0,Indice2=0,Indice3=0;
		StackLayout stackTxtNetwork= new StackLayout();
		StackLayout stackTxtVision= new StackLayout();
		StackLayout stackTxtMission= new StackLayout();
		StackLayout stack= new StackLayout();

		public AziendaPage (float w,float h)
		{

			var scrollview = new ScrollView {
				Content = stack,
			};


			/*
			AbsoluteLayout absoluteLayout = new AbsoluteLayout
			{
				BackgroundColor = Color.White,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = Height*3,
			};*/

			//this.BackgroundColor = Color.Blue;
			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Back", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.White;
					th.TextColor = Color.White;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));


			//this.BackgroundColor = Color.Blue;
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);

			Image immagine = new Image { Aspect = Aspect.AspectFit };
			immagine.Source = ImageSource.FromFile("immagine_about_us.png");
			immagine.WidthRequest = w;
			immagine.HeightRequest = (w / 481) * 271;

			MyQuicksandRegularLabel Network = new MyQuicksandRegularLabel {
				Text = "CHI SIAMO",
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				TextColor=Color.White,
				HeightRequest= 30,
				WidthRequest = w,
				BackgroundColor= Color.FromHex("3D3D3F"),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				//FontAttributes = FontAttributes.Bold
			};

			var tapGestureRecognizer = new TapGestureRecognizer ();
			tapGestureRecognizer.Tapped += OnLabelTap;
			Network.GestureRecognizers.Add (tapGestureRecognizer);

			TestoNetwork = new MyQuicksandRegularLabel {
				Text = "Siamo un gruppo di professionisti consolidato in anni di collaborazioni, da sempre tutti operiamo nel settore informatico, ognuno con il proprio percorso lavorativo e il proprio bagaglio di conoscenza, e accomunati tutti dalla vocazione per quelle tecnologie in grado di creare emozioni.\n \nCi siamo incontrati e chiesti se fosse possibile creare un network che potesse rispondere al mercato con affidabilità, competenza, rapidità e, non ultimo, con un occhio di riguardo ai costi.\n \nCosì è nata Netwintec (Network Innovazione Tecnologica).",
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				//WidthRequest=w-20,
				//TranslationX = 10,
				//BackgroundColor = Color.Aqua,
				//Font = Font.SystemFontOfSize (NamedSize.Medium),
			};

			//TestoNetwork.Layout(new Rectangle (10,0,w-20,TestoNetwork.Height));

			MyQuicksandRegularLabel Mission = new MyQuicksandRegularLabel {
				Text = "MISSION",
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				TextColor=Color.White,
				HeightRequest= 30,
				WidthRequest = w,
				BackgroundColor= Color.FromHex("3D3D3F"),
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				//FontAttributes = FontAttributes.Bold
			};

			var tapGestureRecognizer2 = new TapGestureRecognizer ();
			tapGestureRecognizer2.Tapped += OnLabelTap2;
			Mission.GestureRecognizers.Add (tapGestureRecognizer2);

			TestoMission = new MyQuicksandRegularLabel {
				Text = "La nostra tecnologia al servizio delle tue emozioni e la continua ricerca di un idea brillante al servizio del tuo Business, questa è la nostra Mission, e per ottenere il risultato dobbiamo coinvolgere il cliente invitandolo a manifestare le proprie necessità, le proprie esigenze e le proprie idee, al fine di creare quell’emozione finale che sarà determinante per il raggiungimento dell’obiettivo e della soddisfazione.\n \nCrediamo fortemente nelle innovazioni tecnologiche, investiamo in ricerca e sviluppo per individuare quelle soluzioni che potranno essere vincenti per il business.\n \nIl nostro cliente tipo è quindi colui che ha capito il significato di “investire nel futuro”.",
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				//Font = Font.SystemFontOfSize (NamedSize.Medium),
			};

			MyQuicksandRegularLabel Vision = new MyQuicksandRegularLabel {
				Text = "VISION",
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				TextColor=Color.White,
				HeightRequest= 30,
				WidthRequest = w,
				BackgroundColor= Color.FromHex("3D3D3F"),

				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				//FontAttributes = FontAttributes.Bold
			};

			var tapGestureRecognizer3 = new TapGestureRecognizer ();
			tapGestureRecognizer3.Tapped += OnLabelTap3;
			Vision.GestureRecognizers.Add (tapGestureRecognizer3);

			TestoVision = new MyQuicksandRegularLabel {
				Text = "Consideriamo la tecnologia come uno strumento per vivere nuove esperienze, nuove emozioni.",
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				//Font = Font.SystemFontOfSize (NamedSize.Medium),
			};

			MyQuicksandRegularLabel Sito = new MyQuicksandRegularLabel {
				Text = "Vai al Sito",
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				BackgroundColor= Color.Transparent,
				//TextColor=Color.White,
				//HeightRequest= 30,
				//WidthRequest = w,
				HorizontalOptions=LayoutOptions.Center,
				TextColor= Color.FromHex("3D3D3F"),
				//Font = Font.SystemFontOfSize(NamedSize.Medium),
				//FontAttributes = FontAttributes.Bold
			};
			var tapGestureRecognizerLabel = new TapGestureRecognizer ();
			tapGestureRecognizerLabel.Tapped += delegate {

				//stack.Children.Remove (stackTxtNetwork);
				Navigation.PushAsync(new SitoPage("NetWinTec","http://www.netwintec.com"));

			};
			Sito.GestureRecognizers.Add (tapGestureRecognizerLabel);


			stackTxtNetwork.Padding = new Thickness (20, 0, 20, 0);
			//stackTxtNetwork.Children.Add (TestoNetwork);
			stackTxtMission.Padding = new Thickness (20, 0, 20, 0);
			//stackTxtMission.Children.Add (TestoMission);
			stackTxtVision.Padding = new Thickness (20, 0, 20, 0);
			//stackTxtVision.Children.Add (TestoVision);

			MyQuicksandRegularLabel speak = new MyQuicksandRegularLabel {
				Text = "Text To Speech!",
				FontFamily = Device.OnPlatform (
					"Quicksand",
					null,
					null
				),
				BackgroundColor= Color.Transparent,
				//TextColor=Color.White,
				//HeightRequest= 30,
				//WidthRequest = w,
				//VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Center,
				TextColor= Color.FromHex("3D3D3F"),
				//Font = Font.SystemFontOfSize(NamedSize.Medium),
				//FontAttributes = FontAttributes.Bold
			};
			int i = 0;

			var tapGestureRecognizerLabel2 = new TapGestureRecognizer ();
			tapGestureRecognizerLabel2.Tapped += delegate {
				
				System.Diagnostics.Debug.WriteLine("1");

				if(i % 2 == 0){
					System.Diagnostics.Debug.WriteLine("2");
					DependencyService.Get<ITextToSpeech>().Speak("Siamo un gruppo di professionisti consolidato in anni di collaborazioni, da sempre tutti operiamo nel settore informatico, ognuno con il proprio percorso lavorativo e il proprio bagaglio di conoscenza, e accomunati tutti dalla vocazione per quelle tecnologie in grado di creare emozioni. Crediamo fortemente nelle innovazioni tecnologiche, investiamo in ricerca e sviluppo per individuare quelle soluzioni che potranno essere vincenti per il business.");
				}
				if(i % 2 == 1){
					System.Diagnostics.Debug.WriteLine("3");
					DependencyService.Get<ITextToSpeech>().Stop();
				}
				i++;

			};
			speak.GestureRecognizers.Add (tapGestureRecognizerLabel2);
			/*speak.Clicked += (sender, e) => {
				if(i == 0){
					i++;
					DependencyService.Get<ITextToSpeech>().Speak("Siamo un gruppo di professionisti consolidato in anni di collaborazioni, da sempre tutti operiamo nel settore informatico, ognuno con il proprio percorso lavorativo e il proprio bagaglio di conoscenza, e accomunati tutti dalla vocazione per quelle tecnologie in grado di creare emozioni. Crediamo fortemente nelle innovazioni tecnologiche, investiamo in ricerca e sviluppo per individuare quelle soluzioni che potranno essere vincenti per il business.");
				}
				/*else{
					if(i%2==1){
						//DependencyService.Get<ITextToSpeech>().StopSpeak();
					}
					if(i%2==0){
						//DependencyService.Get<ITextToSpeech>().ResumeSpeak();
					}
					i++;
				}
			};
			*/



			stack.Children.Add (immagine);
			stack.Children.Add (Network);
			stack.Children.Add (stackTxtNetwork);
			stack.Children.Add (Vision);
			stack.Children.Add (stackTxtVision);
			stack.Children.Add (Mission);
			stack.Children.Add (stackTxtMission);
			stack.Children.Add (Sito);
			stack.Children.Add (speak);



			//stack.Padding = new Thickness (20, 0, 20, 0);

			this.Content = scrollview;


			this.Appearing += (sender, e) =>
			{
				System.Diagnostics.Debug.WriteLine ("App:"+TestoNetwork.Height + "      " + TestoMission.Height + "      " + TestoVision.Height);
			};


		}

		void OnLabelTap(object sender, EventArgs args){
			if (Indice1 % 2 == 0) {
				stackTxtNetwork.Children.Add (TestoNetwork);
			} else {
				stackTxtNetwork.Children.Remove (TestoNetwork);
			}
			Indice1++;
		}

		void OnLabelTap2(object sender, EventArgs args){
			if (Indice2 % 2 == 0) {
				stackTxtMission.Children.Add (TestoMission);
			} else {
				stackTxtMission.Children.Remove(TestoMission);
			}
			Indice2++;
		}

		void OnLabelTap3(object sender, EventArgs args){
			if (Indice3 % 2 == 0) {
				stackTxtVision.Children.Add (TestoVision);
			} else {
				stackTxtVision.Children.Remove (TestoVision);
			}
			Indice3++;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			System.Diagnostics.Debug.WriteLine ("OnApp:"+TestoNetwork.Height + "      " + TestoMission.Height + "      " + TestoVision.Height);
		}
	}
}

