﻿using System;

namespace NetForYouDroid
{
	public interface ITextToSpeech
	{
		void Speak(string text);
		void Stop();
	}
}

