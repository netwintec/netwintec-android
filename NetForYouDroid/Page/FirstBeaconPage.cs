﻿using System;
using Xamarin.Forms;

namespace NetForYouDroid
{
	public class FirstBeaconPage : ContentPage
	{
		public FirstBeaconPage ()
		{
			StackLayout stack= new StackLayout();


			NavigationPage.SetHasBackButton (this, false);
			//NavigationPage.SetBackButtonTitle (this, "");

			ToolbarItems.Add(new ToolbarItem("Back", "tasto_indietro_header.png", async () =>
				{
					//await Navigation.PushModalAsync(new RetHomePage());
					TornaHome th = new TornaHome();
					th.BackgroundColor = Color.White;
					th.TextColor = Color.White;
					stack.Children.Add(th);
					//var result = await this.DisplayAlert("Title", "Message", "Accept", "Cancel");
					//System.Diagnostics.Debug.WriteLine("aa");

				}));


			//this.BackgroundColor = Color.Blue;
			var s = "logo_header_1.png";
			NavigationPage.SetTitleIcon (this, s);

			Image immagine = new Image { Aspect = Aspect.AspectFit };
			immagine.Source = ImageSource.FromFile("netwintec.jpg");

			stack.Children.Add (immagine);

			stack.Padding = new Thickness (10, 10, 10, 0);

			this.Content = stack;


		}
	}
}

