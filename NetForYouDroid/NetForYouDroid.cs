﻿using System;

using Xamarin.Forms;

namespace NetForYouDroid
{
	public class App : Application
	{
		static volatile App _Instance;
		static object _SyncRoot = new Object();
		public static App Instance
		{
			get 
			{
				if (_Instance == null) 
				{
					lock (_SyncRoot) 
					{
						if (_Instance == null) {
							_Instance = new App ();
							_Instance.OAuthSettings = 
								new OAuthSettings (
									clientId: "584261008377345",  		// your OAuth2 client id 
									scope: "",  		// The scopes for the particular API you're accessing. The format for this will vary by API.
									authorizeUrl: "https://www.facebook.com/dialog/oauth/",
									redirectUrl: "http://test.keepup.pro/success.html");   // the redirect URL for the service

							// If you'd like to know more about how to integrate with an OAuth provider, 
							// I personally like the Instagram API docs: http://instagram.com/developer/authentication/
						}
					}
				}

				return _Instance;
			}
		}

		public OAuthSettings OAuthSettings { get; private set; }

		NavigationPage _NavPage;

		public static MasterDetailPage MDPage;

		/*public Page GetMainPage(float widht,float height)
		{
			return new NavigationPage(new HomePage(widht,height));
		}*/


		public  Page GetMasterDetailPage(float widht,float height){
			var md = new MasterDetailPage ();

			md.Master = new MainDetailPage (md,widht,height);
			md.Detail = new NavigationPage(new HomePage (widht,height)) {Tint = App.NavTint};

			return md;
		}

		public  Page GetMasterDetailPageOnProdotti(float widht,float height){
			var md = new MasterDetailPage ();

			md.Master = new MainDetailPage (md,widht,height);
			md.Detail = new NavigationPage(new ProdottiPage (widht,height)) {Tint = App.NavTint};

			return md;
		}

		public  Page GetMasterDetailPageOnAzienda(float widht,float height){
			var md = new MasterDetailPage ();

			md.Master = new MainDetailPage (md,widht,height);
			md.Detail = new NavigationPage(new AziendaPage (widht,height)) {Tint = App.NavTint};

			return md;
		}

		public static Color NavTint {
			get {
				return Color.FromHex ("FFFFFF"); // Xamarin Blue
			}
		}

		public static Color HeaderTint {
			get {
				return Color.FromHex ("FFFFFF"); // Xamarin DarkBlue
			}
		}

		public Page GetBeaconMainPage (string titolo, string urlimg, string descr, string action,int n,int h,int w)
		{
			System.Diagnostics.Debug.WriteLine (n);
			//var md = new MasterDetailPage ();

			//md.Master = new MainDetailPage (md,w,h);
			//md.Detail = new NavigationPage(new AziendaPage (widht,height)) {Tint = App.NavTint};

			if (n == 8) {
				return new NavigationPage (new BenvenutoPage (h,w));
			}
			if (n == 10) {
				return new NavigationPage (new LinkedinPage ());
			}
			if (n == 1) {
				return new NavigationPage (new SecondBeaconPage ());
			}
			if (n == 3) {
				return new NavigationPage (new SitoPageBeacon ());
			}
			return null;
		}
			
		public Page GetFirstMainPage (float widht,float height)
		{
			return new NavigationPage(new MainLoginPage(widht, height));
			//return new NavigationPage(new VideoBeaconPage());

		}

		public bool IsAuthenticated {
			get { return !string.IsNullOrWhiteSpace(_Token); }
		}

		bool _Giaffatto=false;
		public bool Giaffatto {
			get { return _Giaffatto; }
		}

		public void SaveFatto(bool giaffatto)
		{
			_Giaffatto = giaffatto;

		}

		bool _BeacPage=false;
		public bool BeacPage {
			get { return _BeacPage; }
		}

		public void SaveBeaconPage(bool beacPage)
		{
			_BeacPage = beacPage;

		}

		int _Prec;
		public int Prec {
			get { return _Prec; }
		}

		public void SavePrec(int prec)
		{
			_Prec = prec;
			System.Diagnostics.Debug.WriteLine (_Prec);

		}

		int _Beacon;
		public int Beacon {
			get { return _Beacon; }
		}

		public void SaveBeacon(int beac)
		{
			_Beacon = beac;
			System.Diagnostics.Debug.WriteLine (_Beacon);

		}


		string _Token;
		public string Token {
			get { return _Token; }
		}

		public void SaveToken(string token)
		{
			_Token = token;

			// broadcast a message that authentication was successful
			MessagingCenter.Send<App> (this, "Authenticated");
		}

		string _TokenN4U;
		public string TokenN4U {
			get { return _TokenN4U; }
		}

		public void SaveTokenN4U(string tokenN4U)
		{
			_TokenN4U = tokenN4U;

			// broadcast a message that authentication was successful
			//MessagingCenter.Send<App> (this, "Authenticated");
		}

		string _Name;
		public string Name {
			get { return _Name; }
		}

		public void SaveName(string name)
		{
			_Name = name;

			// broadcast a message that authentication was successful
			//MessagingCenter.Send<App> (this, "Authenticated");
		}

		bool _Scan;
		public bool Scan {
			get { return _Scan; }
		}

		public void SaveScan(bool scan)
		{
			_Scan =  scan;

			// broadcast a message that authentication was successful
			//MessagingCenter.Send<App> (this, "Authenticated");
		}

		bool _ScanBeIn;
		public bool ScanBeIn {
			get { return _ScanBeIn; }
		}

		public void SaveScanBeIn(bool scanBeIn)
		{
			_ScanBeIn =  scanBeIn;

			// broadcast a message that authentication was successful
			//MessagingCenter.Send<App> (this, "Authenticated");
		}

		string _tit1,_url1,_descr1,_data1;
		string _tit2,_url2,_descr2,_data2;
		public string Tit1 {
			get { return _tit1; }
		}

		public string URL1 {
			get { return _url1; }
		}

		public string Descr1 {
			get { return _descr1; }
		}

		public string Data1 {
			get { return _data1; }
		}

		public string Tit2 {
			get { return _tit2; }
		}

		public string URL2 {
			get { return _url2; }
		}

		public string Descr2 {
			get { return _descr2; }
		}

		public string Data2 {
			get { return _data2; }
		}

		public void SaveNews1(string tit1,string url1,string descr1,string data1)
		{
			_tit1 = tit1;
			_url1 = url1;
			_descr1 = descr1;
			_data1 = data1;
		}

		public void SaveNews2(string tit2,string url2,string descr2,string data2)
		{
			_tit2 =  tit2;
			_url2 = url2;
			_descr2 = descr2;
			_data2 = data2;
		}

		bool _News;
		public bool News {
			get { return _News; }
		}

		public void SaveNews(bool News)
		{
			_News =  News;

			// broadcast a message that authentication was successful
			//MessagingCenter.Send<App> (this, "Authenticated");
		}

	}
}

