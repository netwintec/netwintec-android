﻿using System;
using Xamarin.Forms.Maps;
using Android.App;
using Android.Content.PM;
using System.Collections.Generic;
using Android.Views;
using Android.Bluetooth;
using Android.OS;
using RestSharp;
using System.Json;
using System.Web;
using System.Text.RegularExpressions;
using RadiusNetworks.IBeaconAndroid;
using Android.Content;
using Newtonsoft.Json.Linq;

namespace NetForYouDroid.Droid
{
	[Activity (Label = "NetForYou",Theme="@android:style/Theme.Holo.Light",  ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity,IBeaconConsumer
	{
		public Bundle b;
		private const string UUID = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";
		private const string beaconId = "BlueBeacon";

		bool _paused;
		IBeaconManager _iBeaconManager;
		View _view;
		MonitorNotifier _monitorNotifier;
		RangeNotifier _rangeNotifier;
		Region _monitoringRegion;
		Region _rangingRegion;
		Dictionary <string,bool> flagBeacon = new Dictionary <string,bool> ();
		bool flagTokenN4U = true;
		public Dictionary<string,Beacon> ListBeacon = new Dictionary<string,Beacon> ();
		public List<Messaggio> ListMessaggi = new List <Messaggio> ();
		public int i=0,z=0,countSleep=51,countBeIn=0;
		public bool Flag401,flagBeIn=true;
		public ISharedPreferences prefs;
		private static Dictionary<string, List<int>> ListBeaconBeIn = new Dictionary<string,List<int>> ();
		public static List<string> BeaconNetworks = new List<string>();

		public MainActivity()
		{
			_iBeaconManager = IBeaconManager.GetInstanceForApplication(this);

			_monitorNotifier = new MonitorNotifier();
			_rangeNotifier = new RangeNotifier();

			_monitoringRegion = new Region(beaconId, UUID, null, null);
			_rangingRegion = new Region(beaconId, UUID, null, null);

		}

		protected override void OnCreate (Bundle bundle)
		{
			b = bundle;

			MainActivity.Self = this;

			base.OnCreate (bundle);

			Xamarin.FormsMaps.Init (this, bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);

			var metrics = Resources.DisplayMetrics;
			var widthInDp = (int)(metrics.WidthPixels / Resources.DisplayMetrics.Density);
			var heightInDp = (int)(metrics.HeightPixels / Resources.DisplayMetrics.Density);

			Console.WriteLine (metrics.WidthPixels + "    " + metrics.HeightPixels + "    " + widthInDp + "       " + heightInDp);      
			if (App.Instance.BeacPage == true) {
				
				SetPage (App.Instance.GetBeaconMainPage ("", "", "", "", App.Instance.Beacon, widthInDp, heightInDp));

			} else {
				if (App.Instance.Giaffatto == false) {
				
					prefs = Application.Context.GetSharedPreferences ("NetForYou", FileCreationMode.Private);

					var somePref = prefs.GetString ("TokenN4U", null);
					Console.WriteLine ("TokenN4u:\"" + somePref + "\" " + (somePref == null));


					if (somePref != null) {
						var client2 = new RestClient ("http://api.netwintec.com/");
						var requestMessage2 = new RestRequest ("active-messages", Method.GET);
						requestMessage2.AddHeader ("content-type", "application/json");
						requestMessage2.AddHeader ("Net4U-Company", "azienda");
						requestMessage2.AddHeader ("Net4U-Token", prefs.GetString ("TokenN4U", null));

						IRestResponse response = client2.Execute (requestMessage2);

						Console.WriteLine ("RESPONSE CODE:" + response.StatusCode);

						if (response.StatusCode == System.Net.HttpStatusCode.OK) {
							Flag401 = true;
							App.Instance.SaveTokenN4U (prefs.GetString ("TokenN4U", null));

							var client3 = new RestClient("http://www.netwintec.com/");
							var requestMessage = new RestRequest("wp-json/posts", Method.GET);
							requestMessage.AddHeader("content-type", "application/json");

							IRestResponse response2 = client3.Execute(requestMessage);

							//string prova = "";

							Console.WriteLine("1 ..>" + "{\"post\":" + response2.Content + "}");
							//Console.WriteLine("..>" + "{\"post\":" + prova + "}");

							JsonValue json = JsonValue.Parse("{\"post\":" + response2.Content + "}");
							Console.WriteLine("JSON:" + json.ToString());
							System.Json.JsonArray data = (System.Json.JsonArray)json["post"];
							Console.WriteLine("data:" + data.ToString());

							int z = 0;
							foreach (JsonValue dataItem in data)
							{
								var titolo = dataItem["title"];
								Console.WriteLine("titolo:" + titolo.ToString());
								var descr = dataItem["content"];
								Console.WriteLine("drc:" + descr.ToString());
								var app = dataItem["featured_image"];
								var urlimg = app["guid"];
								Console.WriteLine("imm:" + urlimg.ToString());
								var date = dataItem["date"];
								Console.WriteLine("DATE:" + date.ToString());
								string appo = date.ToString();
								String[] DataNews = appo.Split('-');
								string annoApp = DataNews[0];
								string anno = annoApp.Substring(1);
								string mese = DataNews[1];
								string appo2 = DataNews[2];
								String[] GiornoNews = appo2.Split('T');
								string giorno = GiornoNews[0];


								string dataBlog = giorno + "/" + mese + "/" + anno;
								Console.WriteLine(dataBlog + " | " + titolo + " | " + urlimg + " | " + descr);


								z++;
								if (z == 1)
								{
									App.Instance.SaveNews1(Regex.Replace(HttpUtility.HtmlDecode(titolo), "<.*?>", ""), urlimg, Regex.Replace(HttpUtility.HtmlDecode(descr), "<.*?>", ""), dataBlog);
								}
								if (z == 2)
								{
									App.Instance.SaveNews2(Regex.Replace(HttpUtility.HtmlDecode(titolo), "<.*?>", ""), urlimg, Regex.Replace(HttpUtility.HtmlDecode(descr), "<.*?>", ""), dataBlog);
									break;
								}
							}

							SetPage (App.Instance.GetMasterDetailPage (widthInDp, heightInDp));



						} else {

							Console.WriteLine ("Token Scaduto");
							Flag401 = false;

						}
					} else {

						var client3 = new RestClient ("http://www.netwintec.com/");
						var requestMessage = new RestRequest ("wp-json/posts", Method.GET);
						requestMessage.AddHeader ("content-type", "application/json");

						IRestResponse response2 = client3.Execute (requestMessage);

						//string prova = "";

						Console.WriteLine("2 ..>" + "{\"post\":" + response2.Content + "}");
						//Console.WriteLine("..>" + "{\"post\":" + prova + "}");

						JsonValue json = JsonValue.Parse ("{\"post\":"+response2.Content+"}");
						Console.WriteLine ("JSON:"+json.ToString());
						System.Json.JsonArray data = (System.Json.JsonArray)json["post"];
						Console.WriteLine("data:" + data.ToString());

						int z = 0;    
						foreach (JsonValue dataItem in data) {
							var titolo = dataItem ["title"];
							Console.WriteLine("titolo:" + titolo.ToString());
							var descr = dataItem ["content"];
							Console.WriteLine("drc:" + descr.ToString());
							var app = dataItem["featured_image"];
							var urlimg = app ["guid"];
							Console.WriteLine("imm:" + urlimg.ToString());
							var date = dataItem ["date"];
							Console.WriteLine ("DATE:"+date.ToString ());
							string appo = date.ToString ();
							String[] DataNews = appo.Split ('-');
							string annoApp = DataNews [0];
							string anno = annoApp.Substring (1);
							string mese = DataNews [1];
							string appo2 = DataNews [2];
							String[] GiornoNews = appo2.Split ('T');
							string giorno = GiornoNews [0];


							string dataBlog = giorno + "/" + mese + "/" + anno;
							Console.WriteLine (dataBlog + " | " + titolo + " | " + urlimg + " | " + descr);


							z++;
							if (z == 1) {
								App.Instance.SaveNews1 (Regex.Replace (HttpUtility.HtmlDecode (titolo), "<.*?>", ""), urlimg, Regex.Replace (HttpUtility.HtmlDecode (descr), "<.*?>", ""), dataBlog);
							}
							if (z == 2) {
								App.Instance.SaveNews2 (Regex.Replace (HttpUtility.HtmlDecode (titolo), "<.*?>", ""), urlimg, Regex.Replace (HttpUtility.HtmlDecode (descr), "<.*?>", ""), dataBlog);
								break;
							}
						}

						SetPage (App.Instance.GetFirstMainPage (widthInDp, heightInDp));

					}

					if (Flag401 == false) {

						var client3 = new RestClient("http://www.netwintec.com/");
						var requestMessage = new RestRequest("wp-json/posts", Method.GET);
						requestMessage.AddHeader("content-type", "application/json");

						IRestResponse response2 = client3.Execute(requestMessage);

						//string prova = "";

						Console.WriteLine("3 ..>" + "{\"post\":" + response2.Content + "}");
						//Console.WriteLine("..>" + "{\"post\":" + prova + "}");

						JsonValue json = JsonValue.Parse("{\"post\":" + response2.Content + "}");
						Console.WriteLine("JSON:" + json.ToString());
						System.Json.JsonArray data = (System.Json.JsonArray)json["post"];
						Console.WriteLine("data:" + data.ToString());

						int z = 0;
						foreach (JsonValue dataItem in data)
						{
							var titolo = dataItem["title"];
							Console.WriteLine("titolo:" + titolo.ToString());
							var descr = dataItem["content"];
							Console.WriteLine("drc:" + descr.ToString());
							var app = dataItem["featured_image"];
							var urlimg = app["guid"];
							Console.WriteLine("imm:" + urlimg.ToString());
							var date = dataItem["date"];
							Console.WriteLine("DATE:" + date.ToString());
							string appo = date.ToString();
							String[] DataNews = appo.Split('-');
							string annoApp = DataNews[0];
							string anno = annoApp.Substring(1);
							string mese = DataNews[1];
							string appo2 = DataNews[2];
							String[] GiornoNews = appo2.Split('T');
							string giorno = GiornoNews[0];


							string dataBlog = giorno + "/" + mese + "/" + anno;
							Console.WriteLine(dataBlog + " | " + titolo + " | " + urlimg + " | " + descr);


							z++;
							if (z == 1)
							{
								App.Instance.SaveNews1(Regex.Replace(HttpUtility.HtmlDecode(titolo), "<.*?>", ""), urlimg, Regex.Replace(HttpUtility.HtmlDecode(descr), "<.*?>", ""), dataBlog);
							}
							if (z == 2)
							{
								App.Instance.SaveNews2(Regex.Replace(HttpUtility.HtmlDecode(titolo), "<.*?>", ""), urlimg, Regex.Replace(HttpUtility.HtmlDecode(descr), "<.*?>", ""), dataBlog);
								break;
							}
						}

						SetPage (App.Instance.GetFirstMainPage (widthInDp, heightInDp));


					}
					



					AlertDialog.Builder builder = new AlertDialog.Builder (this);
					builder.SetTitle ("Bluetooth down");
					builder.SetMessage ("Perfavore Abilitare il Bluetooth.");
					builder.SetCancelable (false);
					builder.SetPositiveButton ("OK", delegate {
					});

					BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;

					if (!bluetoothAdapter.IsEnabled) {
						builder.Show ();
					}


					_iBeaconManager.Bind (this);

					_iBeaconManager.SetMonitorNotifier (_monitorNotifier);
					_iBeaconManager.SetRangeNotifier (_rangeNotifier);

					_monitorNotifier.EnterRegionComplete += EnteredRegion;
					_monitorNotifier.ExitRegionComplete += ExitedRegion;

					Console.WriteLine ("parto2");

					_rangeNotifier.DidRangeBeaconsInRegionComplete += RangingBeaconsInRegion;
				} else {
					Console.WriteLine (App.Instance.Prec);
					if (App.Instance.Prec == 0) {
						SetPage (App.Instance.GetMasterDetailPage (widthInDp, heightInDp));
					}
					if (App.Instance.Prec == 1) {
						SetPage (App.Instance.GetMasterDetailPageOnAzienda (widthInDp, heightInDp));
					}
					if (App.Instance.Prec == 2) {
						SetPage (App.Instance.GetMasterDetailPageOnProdotti (widthInDp, heightInDp));
					}
					if (App.Instance.Prec == 3) {
						SetPage (App.Instance.GetFirstMainPage (widthInDp, heightInDp));
					}
						
				}
			}
				
		}

		public static MainActivity Self {get;private set;}

		public void MasterDetailPage(){

			var metrics = Resources.DisplayMetrics;
			var widthInDp = (int)(metrics.WidthPixels / Resources.DisplayMetrics.Density);
			var heightInDp = (int)(metrics.HeightPixels / Resources.DisplayMetrics.Density);

			global::Xamarin.Forms.Forms.Init (this, b);

			SetPage(App.Instance.GetMasterDetailPage(widthInDp,heightInDp));

		}

		public void FirstLoginPage(){

			var metrics = Resources.DisplayMetrics;
			var widthInDp = (int)(metrics.WidthPixels / Resources.DisplayMetrics.Density);
			var heightInDp = (int)(metrics.HeightPixels / Resources.DisplayMetrics.Density);

			global::Xamarin.Forms.Forms.Init (this, b);

			SetPage(App.Instance.GetFirstMainPage(widthInDp,heightInDp));

		}

		public override void OnBackPressed ()
		{
			Console.WriteLine ("aaa");
		}

		public void BeaconPage(int minor,int height,int width){
			global::Xamarin.Forms.Forms.Init (this, b);
			SetPage (App.Instance.GetBeaconMainPage ("", "","", "", minor, height, width));
		}
		protected override void OnResume() 
		{
			base.OnResume();
			_paused = false;
		}

		public override void OnLowMemory() 
		{
			GC.Collect ();
		}

		protected override void OnPause()
		{
			base.OnPause();
			_paused = true;
		}

		void EnteredRegion(object sender, MonitorEventArgs e)
		{
			//if(_paused)
			//{
			//	ShowNotification(1);
			//}
		}

		void ExitedRegion(object sender, MonitorEventArgs e)
		{
		}

		void RangingBeaconsInRegion(object sender, RangeEventArgs e){
			Console.WriteLine("Token:"+App.Instance.TokenN4U+ "   "+(App.Instance.TokenN4U == null)+"      "+App.Instance.Scan);
			if (flagTokenN4U == true) {
				if (App.Instance.TokenN4U != null) {

					var client = new RestClient ("http://api.netwintec.com/");
					var requestMessage = new RestRequest ("active-messages", Method.GET);
					requestMessage.AddHeader ("content-type", "application/json");
					requestMessage.AddHeader ("Net4U-Company", "azienda");
					requestMessage.AddHeader ("Net4U-Token", App.Instance.TokenN4U);

					IRestResponse response = client.Execute (requestMessage);

					//string prova = "{\"messages\":[{\"title\":\"titolo\",\"description\":\"descrizione\",\"image_url\":\"/image/4fae906a-0e25-4f1d-953c-6d8cf1cc0219\",\"content_url\":\"url\",\"type\":\"ibeacon\",\"timestamp_start\":1429800825,\"timestamp_end\":1450882425,\"distance\":5,\"_type\":\"message\",\"_key\":\"message::e68461b7-13f9-4296-856e-3bf83ffd587a\"}]}";
					Console.WriteLine ("RESPONSE:"+response.Content);
					JsonValue json = JsonValue.Parse (response.Content);

					JsonValue data = json ["messages"];

					foreach (JsonValue dataItem in data) {
						var titolo = dataItem ["title"];
						var urlimg = dataItem ["image_url"];
						var descr = dataItem ["description"];
						var action = dataItem ["content_url"];
						var distanzaAp = dataItem ["threshold"];
						float distanzaFl=0;
						if (distanzaAp.ToString ().Contains (".")) {
							string distStr = distanzaAp.ToString ().Replace ('.', ',');
							distanzaFl = float.Parse (distStr);
						} else {
							distanzaFl = float.Parse (distanzaAp.ToString ());
						}
						Console.WriteLine (distanzaFl);
						int distanza = (int)(distanzaFl*100);
						Console.WriteLine (distanza);
						var key = dataItem ["_key"];

						Console.WriteLine (titolo + "  " + urlimg + "   " + descr + "   " + action);

						ListMessaggi.Add (new Messaggio (titolo, urlimg, descr, action, false));

						var client2 = new RestClient ("http://api.netwintec.com/");
						var requestBeacon = new RestRequest ("active-messages/" + key, Method.GET);
						requestBeacon.AddHeader ("content-type", "application/json");
						requestBeacon.AddHeader ("Net4U-Company", "azienda");
						requestBeacon.AddHeader ("Net4U-Token", App.Instance.TokenN4U);

						IRestResponse response2 = client2.Execute (requestBeacon);

						Console.WriteLine ("Bho:"+response2.Content);

						//string prova2 = "{\"beacons\":[{\"uuid\": \"ACFD065E-C3C0-11E3-9BBE-1A514932AC01\",\"major\": 101,\"minor\": 5},{\"uuid\": \"ACFD065E-C3C0-11E3-9BBE-1A514932AC01\",\"major\": 101,\"minor\": 4}]}";

						JsonValue json2 = JsonValue.Parse (response2.Content);
						JsonValue data2 = json2 ["beacons"];

						foreach (JsonValue dataItem2 in data2) {
							var uuid2 = dataItem2 ["uuid"];
							var majorAp = dataItem2 ["major"];
							int major = int.Parse (majorAp.ToString ());
							var minorAp = dataItem2 ["minor"];
							int minor = int.Parse (minorAp.ToString ());
							Console.WriteLine (uuid2 + "|" + major + "|" + minor + "|" + distanza);
							string Appoggio;
							Appoggio = major.ToString () + "," + minor.ToString () + "," + i;
							ListBeacon.Add (Appoggio, new Beacon (uuid2, major, minor, distanza, i));
							flagBeacon.Add (Appoggio, false);
							if (flagBeacon.ContainsKey (Appoggio) == true) {
								Console.WriteLine ("key:"+Appoggio);
								Console.WriteLine ("c'è");
							}
						}
						i++;
					}
					flagTokenN4U = false;
					Console.WriteLine ("Fine");
				}
			} else {
				if (App.Instance.ScanBeIn == true) {
					Console.WriteLine("BeIn:"+countBeIn+ "   "+countSleep+"      "+flagBeIn);
					if(countSleep>0){
						if (e.Beacons.Count > 0)
						{
							var s = e.Beacons;
							foreach (IBeacon beacon in s) {
								if (!ListBeaconBeIn.ContainsKey (beacon.Minor.ToString ())) {
									BeaconNetworks.Add (beacon.Minor.ToString ());
									ListBeaconBeIn.Add (beacon.Minor.ToString (), new List<int> ());
									ListBeaconBeIn [beacon.Minor.ToString ()].Add (beacon.Rssi);
								} else {
									ListBeaconBeIn [beacon.Minor.ToString ()].Add (beacon.Rssi);
								}
							}

							countBeIn++;
						}
						if(countBeIn==5){

							JObject oJsonObject = new JObject ();

							JArray oJsonArray = new JArray();



							oJsonObject.Add ("mac_address", "1");
							oJsonObject.Add ("userid", "1");

							Console.WriteLine ("Prova " + BeaconNetworks.Count);

							List<JArray> oJsonArrayList = new List<JArray>();
							List<JObject> oJsonObjectList = new List<JObject>();

							for (int z = 0; z < BeaconNetworks.Count; z++) {
								JObject oJsonObjectApp = new JObject();
								JArray oJsonArrayApp = new JArray ();

								oJsonObjectApp.Add ("mac_address_ap", BeaconNetworks [z]);
								oJsonObjectList.Add(oJsonObjectApp);

								List<int> appoggio;
								appoggio = ListBeaconBeIn[BeaconNetworks [z]];

								if (appoggio.Count == 1) {
									Console.WriteLine(z+"   "+appoggio.Count);
									oJsonArrayApp.Add (appoggio[0].ToString());
									oJsonArrayList.Add(oJsonArrayApp);
									oJsonObjectList[z].Add ("valore_rss", appoggio[0].ToString());
								} else {
									Console.WriteLine(z+"   "+appoggio.Count);
									for (int k = 0; k < appoggio.Count; k++) {
										oJsonArrayApp.Add (appoggio [k]);
									}
									oJsonArrayList.Add(oJsonArrayApp);
									oJsonObjectList[z].Add ("valori_rss", oJsonArrayList[z]);
								}

								oJsonArray.Add (oJsonObjectList[z]);
								appoggio.Clear ();
							}


							oJsonObject.Add ("array_valori", oJsonArray);


							Console.WriteLine(oJsonObject.ToString());

							//*********sviluppo in localhost***********
							//var client = new RestClient("http://192.168.0.162:9960");

							//*******Sviluppo Server *********
							var client = new RestClient("http://location.keepup.pro:8085");



							//client.Authenticator = new HttpBasicAuthenticator(username, password);

							var request = new RestRequest("api/v1/1/position", Method.POST);
							request.AddHeader("content-type", "application/json");

							request.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

							//add parameters for all properties on an object

							// execute the request
							IRestResponse response = client.Execute (request);

							// raw content as string
							string content = response.Content; 

							Console.WriteLine("CONTENT: "+content);

							string[] Risposte; 
							Risposte = content.Split (",".ToCharArray (), 6);

							if (Risposte.Length==1) {

								Console.WriteLine ("Get Position:Server Down");

							}

							if (content.Equals ("ERR") == true) {

								Console.WriteLine ("Get Position:Error");

							} 
							if (Risposte.Length > 1 && content.Equals ("ERR") == false) {

								string ID = "";
								string lat = "";
								string lon = "";

								if (Risposte [0].Contains ("IdLocation")) {
									ID = Risposte [0].Substring (15);
								}
								if (Risposte [0].Contains ("latitudine")) {
									lat = Risposte [0].Substring (15);
								}
								if (Risposte [0].Contains ("longitudine")) {
									lon = Risposte [0].Substring (16);
								}

								if (Risposte [1].Contains ("IdLocation")) {
									ID = Risposte [1].Substring (15);
								}
								if (Risposte [1].Contains ("latitudine")) {
									lat = Risposte [1].Substring (15);
								}
								if (Risposte [1].Contains ("longitudine")) {
									lon = Risposte [1].Substring (16);
								}

								if (Risposte [2].Contains ("IdLocation")) {
									ID = Risposte [2].Substring (15);
								}
								if (Risposte [2].Contains ("latitudine")) {
									lat = Risposte [2].Substring (15);
								}
								if (Risposte [2].Contains ("longitudine")) {
									lon = Risposte [2].Substring (16);
								}

								if (Risposte [3].Contains ("IdLocation")) {
									ID = Risposte [3].Substring (15);
								}
								if (Risposte [3].Contains ("latitudine")) {
									lat = Risposte [3].Substring (15);
								}
								if (Risposte [3].Contains ("longitudine")) {
									lon = Risposte [3].Substring (16);
								}

								if (Risposte [4].Contains ("IdLocation")) {
									ID = Risposte [4].Substring (15);
								}
								if (Risposte [4].Contains ("latitudine")) {
									lat = Risposte [4].Substring (15);
								}
								if (Risposte [4].Contains ("longitudine")) {
									lon = Risposte [4].Substring (16);
								}

								if (Risposte [5].Contains ("IdLocation")) {
									ID = Risposte [5].Substring (15);
									ID = ID.Remove (ID.Length - 1);
								}
								if (Risposte [5].Contains ("latitudine")) {
									lat = Risposte [5].Substring (15);
									lat = lat.Remove (lat.Length - 1);
								}
								if (Risposte [5].Contains ("longitudine")) {
									lon = Risposte [5].Substring (16);
									lon = lon.Remove (lon.Length - 1);
								}

								lat = lat.Replace (".", ",");
								lon = lon.Replace (".", ",");

								double NewLat = double.Parse (lat);
								double NewLon = double.Parse (lon);
								this.RunOnUiThread (() => {
									BeInPageRenderer.Self.AddMarker (NewLat, NewLon);
								});
							}

							countSleep=0;
							countBeIn=0;
						}
					}
					else{countSleep++;}
				} else {
					if (App.Instance.Scan == true) {
						if (e.Beacons.Count > 0) {
						var s2 = e.Beacons;
							foreach (IBeacon beacon in s2) {
							//IBeacon [] beaconArray = new IBeacon();
							//e.Beacons.CopyTo(beaconArray,0);

							//for(int a=0;a<beaconArray.Length;a++){
							//	IBeacon beacon = beaconArray[a]; 
								Console.WriteLine ("   " + beacon.Major.ToString () + "  " + beacon.Minor.ToString () + "  " + beacon.Rssi);
								string App2;
								List<string> BeacAppoggioCount = new List <string> ();

								int countBeac = 0;
								for (int q = 0; q < ListMessaggi.Count; q++) {
									App2 = beacon.Major.ToString () + "," + beacon.Minor.ToString () + "," + q;
									Console.WriteLine ("app2:"+beacon.Major.ToString () + "," + beacon.Minor.ToString () + "," + q + "|" + flagBeacon.ContainsKey (App2));
									if (flagBeacon.ContainsKey (App2) == true) {

										BeacAppoggioCount.Add (App2);
										countBeac++;

									}
								}
								Console.WriteLine (countBeac);
								if (countBeac != 0) {
									int a = 0;
									for (int w = 0; w < countBeac; w++) {
										Beacon BeaconApp;
										BeaconApp = ListBeacon [BeacAppoggioCount [w]];
										Console.WriteLine ("Visto"+BeaconApp.IdMessaggio+":" + ListMessaggi [BeaconApp.IdMessaggio].Visto);
										if (ListMessaggi [BeaconApp.IdMessaggio].Visto == false) {
											int distRssi = 0;

											if(BeaconApp.Distance <=50)
												distRssi = (int)(-60);
											
											if(BeaconApp.Distance >50 && BeaconApp.Distance <=100)
												distRssi = (int)(-67);
											
											if(BeaconApp.Distance >100 && BeaconApp.Distance <=150)
												distRssi = (int)(-75);
											
											if(BeaconApp.Distance >150 && BeaconApp.Distance <=200)
												distRssi = (int)(-82);
											
											if(BeaconApp.Distance >200 && BeaconApp.Distance <=500)
												distRssi = (int)(-90);

											if (beacon.Rssi > distRssi) {
												Messaggio MessaggioApp;
												MessaggioApp = ListMessaggi [BeaconApp.IdMessaggio];
												flagBeacon [BeacAppoggioCount [w]] = true;
												ListMessaggi [BeaconApp.IdMessaggio].Visto = true;

												App.Instance.SaveScan (false);
												App.Instance.SaveBeaconPage (true);
												App.Instance.SaveBeacon ((int)beacon.Minor);
												var metrics = Resources.DisplayMetrics;
												var widthInDp = (int)(metrics.WidthPixels / Resources.DisplayMetrics.Density);
												var heightInDp = (int)(metrics.HeightPixels / Resources.DisplayMetrics.Density);


												//using (var h = new Handler (Looper.MainLooper))
												//h.Post (() => {BeaconPage ((int)beacon.Minor, heightInDp, widthInDp);});
												//Application.SynchronizationContext.Post (_ => {BeaconPage ((int)beacon.Minor, heightInDp, widthInDp);}, null);
												//BeaconPage ((int)beacon.Minor, heightInDp, widthInDp);
												var intent = new Intent(this, typeof(MainActivity));
												this.StartActivity(intent);
												//Finish ();
												//RunOnUiThread (() => {
												//	global::Xamarin.Forms.Forms.Init (this, b);
												//	SetPage (App.Instance.GetBeaconMainPage (MessaggioApp.Titolo, MessaggioApp.UrlImage, MessaggioApp.Descrizione, MessaggioApp.Action, (int)beacon.Minor, heightInDp, widthInDp));
											//	});
												//	global::Xamarin.Forms.Forms.Init (this, b);

												//	SetPage (App.Instance.GetBeaconMainPage (MessaggioApp.Titolo, MessaggioApp.UrlImage, MessaggioApp.Descrizione, MessaggioApp.Action, (int)beacon.Minor,heightInDp,widthInDp));
												//});

												Console.WriteLine (MessaggioApp.Titolo + "   " + MessaggioApp.Descrizione);
												w = ListMessaggi.Count + 1;
												a = e.Beacons.Count + 1;
												//break;
											}
										}
									}
									if(a == e.Beacons.Count + 1)
										break; //esco dal foreach
									
								}
							}
						}
					}
				}
			}
		}

		#region IBeaconConsumer impl
		public void OnIBeaconServiceConnect()
		{
			Console.WriteLine ("Connesso  Al beacon");

			_iBeaconManager.StartMonitoringBeaconsInRegion(_monitoringRegion);
			_iBeaconManager.StartRangingBeaconsInRegion(_rangingRegion);
		}
		#endregion


		protected override void OnDestroy()
		{
			base.OnDestroy();

			_monitorNotifier.EnterRegionComplete -= EnteredRegion;
			_monitorNotifier.ExitRegionComplete -= ExitedRegion;

			_rangeNotifier.DidRangeBeaconsInRegionComplete -= RangingBeaconsInRegion;
			Console.WriteLine ("destroy");
			_iBeaconManager.StopMonitoringBeaconsInRegion(_monitoringRegion);
			_iBeaconManager.StopRangingBeaconsInRegion(_rangingRegion);
			_iBeaconManager.UnBind(this);
		}
	}

	public class Beacon{

		public string UUID;
		public int Major;
		public int Minor;
		public int Distance;
		public int IdMessaggio;

		public Beacon(string uuid, int major, int minor, int distance,int idmex){
			UUID = uuid;
			Major = major;
			Minor = minor;
			Distance = distance;
			IdMessaggio = idmex;
		}

	}

	public class Messaggio{

		public string Titolo;
		public string UrlImage;
		public string Descrizione;
		public string Action;
		public bool Visto;

		public Messaggio(string titolo, string urlimg, string descr, string action,bool visto){
			Titolo= titolo;
			UrlImage = urlimg;
			Descrizione = descr;
			Action = action;
			Visto = visto;
		}

	}
}


