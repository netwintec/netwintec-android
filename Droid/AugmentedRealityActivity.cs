﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Android.Media;
using Wikitude.Architect;
using Android.Locations;

namespace NetForYouDroid.Droid
{
	[Activity (Label = "AugmentedRealityActivity",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class AugmentedRealityActivity : Activity
	{
		protected ArchitectView architectView;

		private const string SAMPLE_WORLD_URL = "samples/3DModel/index.html";

		private const string KEY = "18/vUE0vfSURFLoURPF0bTo1sfpJEOkgKzqEZnE0vloS18Nbcz04BPyk+eEOY1Y3wipBd+M3RWy6iZgSQcPFKf8fLQWeSlXqg9ylrt14UZ5QtUzEXMQoGzeKgaAZxZ6EVFnVx7vlyXbhq7357yJtGTJNeNXLNPVWhV/mQ9YOf+VTYWx0ZWRfX9cuWHznY8yPDh86njYNt7lZGbkOiakYQutoLx+Kq2ch/sEXpQCcpB3Gi4bjHrMTZN3JwkxnMk0/2RBzONffO2RChJfy5N98srq4MwKeAQAfowfG83tzme5/PpbL6Gdb77u/G0c3H6lXZ8HascL474TIIGHTMNMjjmg00g7tlJ54hsvZAVS04hrzUoPu+0KXpFdUiI9M+U4NZfQatv7MB1DqLGQNwPgOragYv5GnsK4VFilK4PxSD60Qqgpj76HfTGf+O1Q+mawEpKX7oXmRKUiLa+6Q6xlSbxfR9aOd+p0rSTZhC/0y9paPUfp9LflBnKbv+QkR9ZlYCGY4N+InPgabASBles0qYF4Y+uOGQal2AiQoK27rgCi07X9laeqZ/SOAMwPAF+dWmZa1M8fKJL9f6/5rcpoc98xp9lWOWyzGaBTOGKotoBqPzaYgItr0a63PyGsbtPJkvVuLIgs8JQyT4t6tlG1R3uaf7gGO6GJK6yGMxZZrrWo=";

		private const string TITLE = "NetForYou";

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.sample_cam);

			Title = TITLE;

			architectView = FindViewById<ArchitectView>(Resource.Id.architectView);
			var config = new StartupConfiguration (KEY, StartupConfiguration.Features.Tracking2D);
			/* use  
			   int requiredFeatures = StartupConfiguration.Features.Tracking2D | StartupConfiguration.Features.Geo;
			   if you need both 2d Tracking and Geo
			*/
			int requiredFeatures = StartupConfiguration.Features.Tracking2D | StartupConfiguration.Features.Geo;
			if ((ArchitectView.getSupportedFeaturesForDevice (Android.App.Application.Context) & requiredFeatures) == requiredFeatures) {
				architectView.OnCreate (config);
			} else {
				architectView = null;
			}				
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			if (architectView != null)
				architectView.OnResume ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();

			if (architectView != null)
				architectView.OnPause ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();

			if (architectView != null)
			{
				architectView.OnDestroy ();
				App.Instance.SaveFatto (true);
				App.Instance.SavePrec (0);
				var intent = new Intent(this, typeof(MainActivity));
				StartActivity(intent);
				Finish();

			}
				
		}

		public override void OnLowMemory ()
		{
			base.OnLowMemory ();

			if (architectView != null)
				architectView.OnLowMemory ();
		}

		protected override void OnPostCreate (Bundle savedInstanceState)
		{
			base.OnPostCreate (savedInstanceState);

			if (architectView != null) {
				architectView.OnPostCreate ();

				try {
					architectView.Load (SAMPLE_WORLD_URL);
				} catch (Exception ex) {
					Console.WriteLine ("WIKITUDE_SAMPLE"+ ex.ToString ());
				}
			}
		}


	}

}


