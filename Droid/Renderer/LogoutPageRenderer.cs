﻿using System;
using NetForYouDroid.Droid;
using NetForYouDroid;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;


[assembly: ExportRenderer (typeof (LogoutPage), typeof (LogoutPageRenderer))]

namespace NetForYouDroid.Droid
{	
	public class LogoutPageRenderer : PageRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged (e);
			App.Instance.SaveFatto (true);
			App.Instance.SavePrec (3);			
			var prefs = MainActivity.Self.prefs;
			var prefEditor = prefs.Edit();
			prefEditor.PutString("TokenN4U",null);
			prefEditor.Commit();
			var activity = this.Context as Activity;
			var intent = new Intent(activity, typeof(MainActivity));
			activity.StartActivity(intent);
			//MainActivity.Self.MasterDetailPage();
		}
	}
}

