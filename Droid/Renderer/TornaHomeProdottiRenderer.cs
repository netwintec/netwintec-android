﻿using System;
using NetForYouDroid.Droid;
using NetForYouDroid;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

[assembly: ExportRenderer (typeof (TornaHomeProdotti), typeof (TornaHomeProdottiRenderer))]
namespace NetForYouDroid.Droid
{
	public class TornaHomeProdottiRenderer : LabelRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged (e);
			App.Instance.SaveFatto (true);
			App.Instance.SavePrec (2);
			var activity = this.Context as Activity;
			var intent = new Intent(activity, typeof(MainActivity));
			activity.StartActivity(intent);

		}
	}
}

