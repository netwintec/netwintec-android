﻿using System;
using Xamarin.Forms.Platform.Android;
using NetForYouDroid;
using NetForYouDroid.Droid;
using Xamarin.Forms;
using Android.Widget;
using Android.Graphics;

[assembly: ExportRenderer (typeof (MyQuicksandBoldLabel), typeof (MyQuicksandBoldLabelRenderer))]

namespace NetForYouDroid.Droid
{	
	public class MyQuicksandBoldLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged (e);

			var label = (TextView)Control; // for example
			Typeface font = Typeface.CreateFromAsset (Forms.Context.Assets, "Quicksand_Bold.ttf");
			label.Typeface = font;
		}
	}

}

