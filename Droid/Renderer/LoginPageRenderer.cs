﻿using System; 
using Android.App;
using NetForYouDroid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using NetForYouDroid.Droid;
using Xamarin.Auth;
using RestSharp;
using System.Json;
using System.Web;
using System.Text.RegularExpressions;
using Android.Content;

[assembly: ExportRenderer (typeof (LoginPage), typeof (LoginPageRenderer))]

namespace NetForYouDroid.Droid
{
	public class LoginPageRenderer : PageRenderer
	{

		protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged (e);

			// this is a ViewGroup - so should be able to load an AXML file and FindView<>
			var activity = this.Context as Activity;

			var auth = new Xamarin.Auth.OAuth2Authenticator (
				clientId: App.Instance.OAuthSettings.ClientId, // your OAuth2 client id
				scope: App.Instance.OAuthSettings.Scope, // The scopes for the particular API you're accessing. The format for this will vary by API.
				authorizeUrl: new Uri (App.Instance.OAuthSettings.AuthorizeUrl), // the auth URL for the service
				redirectUrl: new Uri (App.Instance.OAuthSettings.RedirectUrl)); // the redirect URL for the service

			auth.Completed+= ((sender, eventArgs) => {
				
				if (eventArgs.IsAuthenticated) {
					// Use eventArgs.Account to do wonderful things

					App.Instance.SaveToken (eventArgs.Account.Properties["access_token"]);

					Console.WriteLine ("Token:"+App.Instance.Token);

					OAuth2Request request = new OAuth2Request ("GET", new Uri ("https://graph.facebook.com/me"), null, eventArgs.Account);
					Response rs = request.GetResponseAsync ().Result;
					var obj = JsonValue.Parse (rs.GetResponseText ());
					App.Instance.SaveName (obj ["name"]);
					Console.WriteLine ("Scrivo " + App.Instance.Name+"   "+App.Instance.Token);

					var client = new RestClient ("http://api.netwintec.com/");
					//client.Authenticator = new HttpBasicAuthenticator(username, password);

					var requestN4U = new RestRequest ("login/facebook?access_token=" + App.Instance.Token, Method.GET);
					requestN4U.AddHeader ("content-type", "application/json");
					requestN4U.AddHeader ("Net4U-Company", "azienda");

					IRestResponse response = client.Execute (requestN4U);

					string content = response.Content; 

					App.Instance.SaveTokenN4U (content);

					var prefs = MainActivity.Self.prefs;
					var prefEditor = prefs.Edit();
					prefEditor.PutString("TokenN4U",content);
					prefEditor.Commit();

					Console.WriteLine ("TokenN4U:"+content);


					MainActivity.Self.MasterDetailPage();



				}else {
					MainActivity.Self.FirstLoginPage();
				}
			});

			activity.StartActivity (auth.GetUI(activity));
		}
	}
}

