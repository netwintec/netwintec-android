﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Speech.Tts;
using NetForYouDroid;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency (typeof (TextToSpeech_Android))]
public class TextToSpeech_Android : Java.Lang.Object, ITextToSpeech, TextToSpeech.IOnInitListener
{
	TextToSpeech speaker;
	string toSpeak="";

	public TextToSpeech_Android () {}

	public void Speak (string text)
	{
		var ctx = Forms.Context; // useful for many Android SDK features
		toSpeak = text;
		if (speaker == null) {
			speaker = new TextToSpeech (ctx, this);
		} else {
			var p = new Dictionary<string,string> ();
			speaker.Speak (toSpeak, QueueMode.Flush, p);
		}
	}
	public void Stop ()
	{
		if (speaker != null && speaker.IsSpeaking) {
			speaker.Stop ();
		}
	}

	#region IOnInitListener implementation
	public void OnInit (OperationResult status)
	{
		if (status.Equals (OperationResult.Success)) {
			var p = new Dictionary<string,string> ();
			speaker.Speak (toSpeak, QueueMode.Flush, p);
		} 
	}
	#endregion
}

