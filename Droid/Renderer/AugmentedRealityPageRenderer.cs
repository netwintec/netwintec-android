﻿using System;
using Xamarin.Forms.Maps;
using Android.App;
using System;
using NetForYouDroid.Droid;
using NetForYouDroid;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

[assembly: ExportRenderer (typeof (AugmentedReality), typeof (AugmentedRealityPageRenderer))]
namespace NetForYouDroid.Droid
{
	public class AugmentedRealityPageRenderer : PageRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged (e);



			var activity = this.Context as Activity;
			var intent = new Intent (activity, typeof(AugmentedRealityActivity));
			activity.StartActivity (intent);
		}

		/*
		protected ArchitectView architectView;

		private const string SAMPLE_WORLD_URL = "samples/1_Image$Recognition_1_Image$On$Target/index.html";

		private const string KEY = "18/vUE0vfSURFLoURPF0bTo1sfpJEOkgKzqEZnE0vloS18Nbcz04BPyk+eEOY1Y3wipBd+M3RWy6iZgSQcPFKf8fLQWeSlXqg9ylrt14UZ5QtUzEXMQoGzeKgaAZxZ6EVFnVx7vlyXbhq7357yJtGTJNeNXLNPVWhV/mQ9YOf+VTYWx0ZWRfX9cuWHznY8yPDh86njYNt7lZGbkOiakYQutoLx+Kq2ch/sEXpQCcpB3Gi4bjHrMTZN3JwkxnMk0/2RBzONffO2RChJfy5N98srq4MwKeAQAfowfG83tzme5/PpbL6Gdb77u/G0c3H6lXZ8HascL474TIIGHTMNMjjmg00g7tlJ54hsvZAVS04hrzUoPu+0KXpFdUiI9M+U4NZfQatv7MB1DqLGQNwPgOragYv5GnsK4VFilK4PxSD60Qqgpj76HfTGf+O1Q+mawEpKX7oXmRKUiLa+6Q6xlSbxfR9aOd+p0rSTZhC/0y9paPUfp9LflBnKbv+QkR9ZlYCGY4N+InPgabASBles0qYF4Y+uOGQal2AiQoK27rgCi07X9laeqZ/SOAMwPAF+dWmZa1M8fKJL9f6/5rcpoc98xp9lWOWyzGaBTOGKotoBqPzaYgItr0a63PyGsbtPJkvVuLIgs8JQyT4t6tlG1R3uaf7gGO6GJK6yGMxZZrrWo=";

		protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged (e);

			// this is a ViewGroup - so should be able to load an AXML file and FindView<>
			var activity = this.Context as Activity;

			activity.SetContentView(Resource.Layout.sample_cam);

			architectView = FindViewById<ArchitectView>(Resource.Id.architectView);

			//TODO: SDK KEY
			StartupConfiguration config = new StartupConfiguration (KEY, StartupConfiguration.Features.Tracking2D);

			int requiredFeatures = StartupConfiguration.Features.Tracking2D | StartupConfiguration.Features.Geo;
			if ((ArchitectView.getSupportedFeaturesForDevice (Android.App.Application.Context) & requiredFeatures) == requiredFeatures) {
				architectView.OnCreate (config);
			}
			if (architectView != null) {
				architectView.OnPostCreate ();

				try {
					architectView.Load (SAMPLE_WORLD_URL);
				} catch (Exception ex) {
					Console.WriteLine ("WIKITUDE_SAMPLE:"+ ex.ToString ());
				}
			}

			/*
			var config = new ArchitectView.ArchitectConfig (KEY);

			architectView.OnCreate (config);

			this.architectView.RegisterSensorAccuracyChangeListener (this);

			Console.WriteLine ("esiste:" + architectView != null);

			if (architectView != null)
				architectView.OnPostCreate ();

			try
			{
				var world = activity.Intent.Extras.GetString(SAMPLE_WORLD_URL);

				architectView.Load(world);
			}
			catch (Exception ex)
			{
				Console.WriteLine ("WIKITUDE_SAMPLE", ex.ToString ());
			}*/

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);
		}

		protected override void Dispose (bool disposing)
		{
			base.Dispose (disposing);
		}

		protected override void OnAttachedToWindow ()
		{
			base.OnAttachedToWindow ();
		}
		protected override void OnDetachedFromWindow ()
		{
			base.OnDetachedFromWindow ();
		
	
		}
		
	}
}

