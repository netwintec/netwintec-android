﻿using System;
using Xamarin.Forms.Platform.Android;
using NetForYouDroid;
using NetForYouDroid.Droid;
using Xamarin.Forms;
using Android.Widget;
using Android.Graphics;

[assembly: ExportRenderer (typeof (MyQuicksandRegularLabel), typeof (MyQuicksandRegularLabelRenderer))]

namespace NetForYouDroid.Droid
{	
	public class MyQuicksandRegularLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged (e);

			var label = (TextView)Control; // for example
			Typeface font = Typeface.CreateFromAsset (Forms.Context.Assets, "Quicksand_Regular.ttf");
			label.Typeface = font;
		}
	}

}

