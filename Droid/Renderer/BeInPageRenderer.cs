﻿using System; 
using NetForYouDroid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using NetForYouDroid.Droid;
using Android.App;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Xamarin.Forms.Maps;
using System;
using Xamarin.Forms.Maps.Android;

[assembly: ExportRenderer (typeof (MyMap), typeof (BeInPageRenderer))]

namespace NetForYouDroid.Droid
{
	public class BeInPageRenderer : MapRenderer
	{

		public static GoogleMap googleMap;
		public static MapView mapView;
		public Activity activity;
		public MarkerOptions mapMarker;
		MyMap myMap;
		GroundOverlayOptions piani;

		protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);

			BeInPageRenderer.Self = this;

			if (e.OldElement == null) {

				mapView = Control as MapView;

				googleMap = mapView.Map;
				googleMap.UiSettings.CompassEnabled = false;
				googleMap.UiSettings.MyLocationButtonEnabled = false;
				googleMap.MyLocationEnabled = false;
				googleMap.UiSettings.MapToolbarEnabled = false;
				LatLngBounds llb = new LatLngBounds (new LatLng (43.842057, 11.173452), new LatLng (43.842167, 11.173650));
				piani = new GroundOverlayOptions ().InvokeImage (BitmapDescriptorFactory.FromResource (Resource.Drawable.UfficioNWT)).PositionFromBounds (llb);

				googleMap.AddGroundOverlay (piani);

				mapMarker = new MarkerOptions ();
				mapMarker.SetPosition (new LatLng (0, 0));
				googleMap.AddMarker (mapMarker);

				myMap = e.NewElement as MyMap;

				//AddMarker (1,1);

			}

			App.Instance.SaveScanBeIn (true);
		}

		public static BeInPageRenderer Self {get;private set;}

		public void AddMarker (double lat,double lon){
			ClearMap ();
			mapMarker.SetPosition(new LatLng (lat, lon));
			googleMap.AddMarker (mapMarker);
			/*
			mapView.Clear ();

			var overlay = new Google.Maps.GroundOverlay ();
			overlay.Bounds = overlayBounds;
			overlay.Icon = icon;
			overlay.Bearing = 0;
			overlay.Map = mapView;

			var xamMarker = new Marker () {
				Position = new CLLocationCoordinate2D (lat,lon),
				Map = mapView
			};
			mapView.SelectedMarker = xamMarker;
			*/
		}

		public void ClearMap (){
			googleMap.Clear ();
			googleMap.AddGroundOverlay (piani);
		}

	}
	/*
		protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged (e);
			activity = Context as Activity;
			activity.SetContentView (Resource.Layout.MainLayout);

			mapView = FindViewById<MapView> (Resource.Id.map1);
			mapView.OnCreate (MainActivity.Self.b);
			mapView.GetMapAsync (this);


		}

		public async void OnMapReady (GoogleMap googleMap1)
		{
			googleMap = googleMap1;
			googleMap.UiSettings.CompassEnabled = false;
			googleMap.UiSettings.MyLocationButtonEnabled = false;
			googleMap.UiSettings.MapToolbarEnabled = false;
			LatLngBounds llb = new LatLngBounds (new LatLng (43.842077, 11.173881), new LatLng (43.842077, 11.173881));
			var piani = new GroundOverlayOptions ().InvokeImage (BitmapDescriptorFactory.FromFile ("BeIn.png")).PositionFromBounds (llb);

			googleMap.AddGroundOverlay (piani);

			mapView.Clickable = false;

			MapsInitializer.Initialize(activity);
			googleMap.MoveCamera (CameraUpdateFactory.NewLatLng (new LatLng (43.842077, 11.173881)));
		}*/
}

