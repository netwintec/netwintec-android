﻿using System;
using Xamarin.Forms.Platform.Android;
using NetForYouDroid;
using NetForYouDroid.Droid;
using Xamarin.Forms;
using Android.Widget;
using Android.Graphics;

[assembly: ExportRenderer (typeof (MyQuicksandLightLabel), typeof (MyQuicksandLightLabelRenderer))]

namespace NetForYouDroid.Droid
{	
	public class MyQuicksandLightLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged (e);

			var label = (TextView)Control; // for example
			Typeface font = Typeface.CreateFromAsset (Forms.Context.Assets, "Quicksand_Light.ttf");
			label.Typeface = font;
		}
	}

}

