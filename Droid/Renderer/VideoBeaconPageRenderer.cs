﻿using System;
using NetForYouDroid.Droid;
using NetForYouDroid;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Media;

[assembly: ExportRenderer (typeof (VideoBeaconPage), typeof (VideoBeaconPageRenderer))]
namespace NetForYouDroid.Droid
{		

	public class VideoBeaconPageRenderer : PageRenderer,ISurfaceHolderCallback,MediaPlayer.IOnPreparedListener
	{

		VideoView videoView;
		MediaPlayer player;
		int i=0,SecPause=0;
		MediaController mController;
		protected override void OnElementChanged (ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged (e);
			var activity = this.Context as Activity;

			activity.SetContentView (Resource.Layout.VideoBeaconLayout);
		
			videoView = activity.FindViewById<VideoView> (Resource.Id.videoView1);
			//videoView.SetVideoPath("videoBeacon.mp4");
			ISurfaceHolder holder = videoView.Holder;
			holder.SetType (SurfaceType.PushBuffers);
			// Necesito saber cuando la superficie esta creada para poder asignar el Display al MediaPlayer
			holder.AddCallback (this);
			player = new MediaPlayer();
			Android.Content.Res.AssetFileDescriptor afd = activity.Assets.OpenFd("videoBeacon.mp4");
			mController= new MediaController(this.Context);

			/*{
				mController.SetMediaPlayer(this);
				mController.SetAnchorView(videoView); 
				mController.Show();
			};*/

			if (afd != null)
			{
				player.SetDataSource(afd.FileDescriptor, afd.StartOffset, afd.Length);
				
				player.SetVideoScalingMode(VideoScalingMode.ScaleToFit);
				player.Looping = false;
				player.Prepare ();

			}

			player.Completion += delegate {
				Console.WriteLine ("Reset");
				player.Reset();
			};

			videoView.Touch += TouchMeImageViewOnTouch ;
				

		}

		private void TouchMeImageViewOnTouch(object sender,TouchEventArgs touchEventArgs)
		{

			//Console.WriteLine (imageview.GetZ () + "   " + imageview2.GetZ ());
			if (touchEventArgs.Event.Action==MotionEventActions.Up ) {

				if (i%2==0){
					Console.WriteLine ("Start:"+(SecPause)+"ms");
					player.Start();
					player.SeekTo (SecPause);
				}
				if (i%2==1){
					player.Pause();
					SecPause = player.CurrentPosition;
					Console.WriteLine ("Stop:"+(SecPause)+"ms");
				}
				i++;
			}
		}

		public void OnPrepared (MediaPlayer mp)
		{
			throw new NotImplementedException ();
		}

		public void SurfaceCreated (ISurfaceHolder holder)
		{
			Console.WriteLine ("SurfaceCreated");
			player.SetDisplay(holder);
		
		}

		public void SurfaceDestroyed (ISurfaceHolder holder)
		{
			Console.WriteLine ("SurfaceDestroyed");
		}

		public void SurfaceChanged (ISurfaceHolder holder, Android.Graphics.Format format, int w, int h)
		{
			Console.WriteLine ("SurfaceChanged");
		}
	}

}
		
